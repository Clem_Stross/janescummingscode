'''
Created on Dec 4, 2015

@author: cs9114
'''
import numpy
import random
import math

from FormHam import LHII_Structure

from Read_InputFile import ReadFromFile

from ReadPDB import ReadPDB
from ReadPDB import CentreAndRotate_Fuction
from ReadPDB import FindFiles

# TransVecs=numpy.array([[0.0,0.0,0.0],[0.0,70.0,0.0],[0.0,35,60.62]])


def LHII_Ham(InFile):
    
    random.seed()
    
    LHII_Num=ReadFromFile("LHII_Num",InFile,"int",1)
    TransVecs=Makelattices(LHII_Num)
    
    HolderLHII=LHII_Structure()
    
    
    E0=[0.05625,0.05725] #Site energies for B850 and B800
    Mu0=2.87 #TDM magnitude for both B850 and B800
    HamType=ReadFromFile("HamType",InFile,"String")
    
    
    RingNorm=ReadFromFile("RingNorm",InFile,"List",numpy.array([1,0,0]))
    
    for Struct in range(0,LHII_Num,1):
    
        if HamType in ("Real_MD","Real", "MD","RealMD"):
            RingType=ReadFromFile("RingType",InFile,"String")
            lambda_E0=[1.0,0.9]
            lambda_Mu0=1.0
            RingType=ReadFromFile("RingType",InFile,"String")
            DirectoryPath='/Volumes/curie/chmwvdk/projects/lhc2/md/1/'
            FileList=FindFiles(DirectoryPath,".pdb")
            File=random.choice(FileList)    #pick random file from FileList
            Coord=ReadPDB(open(DirectoryPath+File,"r"),RingType,3)    #Save coordinates from pbd 
            Coord=CentreAndRotate_Fuction(Coord)    #Centre and Rotate coordinates, (rotaion is important for 2D electric vector)
            
            WorkingLHII=LHII_Structure()    #make instance of LHII_Structure()
            WorkingLHII.SetAtomicCoord(Coord)   #Set atomic coordinates from choosen pbd file
    
    
        elif HamType=="Ideal":
            N_BChl=ReadFromFile("N_BChl",InFile,"int")
            lambda_E0=[0.0,0.0]
            lambda_Mu0=0
            WorkingLHII=LHII_Structure()
            WorkingLHII.SetAtomicCoord_IdealLHII(N_BChl)
            

        if numpy.linalg.norm(RingNorm)==0 and LHII_Num==1:
            phi=random.uniform(0,math.pi)
            theta=math.acos(random.uniform(-1,1))
            RingNorm=numpy.array([math.cos(phi)*math.sin(theta),math.sin(phi)*math.sin(theta),math.cos(theta)])
        elif LHII_Num>1 and numpy.absolute(numpy.dot(RingNorm,numpy.array([1,0,0])))!=1:
            RingNorm=numpy.array([1,0,0])
            print "Can't use random ring norm and Multiple LHII"
        elif LHII_Num==0:
            print "'LHII_Num' in input set to 0"
            sys.exit()
        
        WorkingLHII.RotateLHII(RingNorm)
        WorkingLHII.SetHamPram(E0,lambda_E0,Mu0,lambda_Mu0)  #set Ei and Mui from chosen distribution
        
        WorkingLHII.Translate_LHII_Structure(TransVecs[Struct])
        HolderLHII.Combine_LHII_Structures(WorkingLHII)
    
    
    
    HolderLHII.FormHam()
    
    return HolderLHII.Ham, numpy.array([HolderLHII.MuNorm[BChl]*HolderLHII.MuMag[BChl] for BChl in range(0,HolderLHII.BChl_Num,1)]),HolderLHII.MgPos



def Makelattices(N):
    
    LatticeSize=int(math.ceil(N**0.5))
    
    d=70
    v1=numpy.array([0,1,0])*d
    v2=numpy.array([0,numpy.cos(numpy.pi/3),numpy.sin(numpy.pi/3)])*d
    
    LatticeSites=numpy.zeros(shape=(N,3))
    
    for i in range(0,LatticeSize,1):
        for j in range(0,LatticeSize,1):
            
            if (i*LatticeSize)+j==N:
                return LatticeSites
            else:
                LatticeSites[(i*LatticeSize)+j]=(v1*j)+(v2*i)
    return LatticeSites
    