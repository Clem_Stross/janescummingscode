'''
Created on Dec 10, 2015

@author: cs9114
'''

import numpy
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import matplotlib.animation as animation

from SitePopulation import RelSitePop

def DrawSystem(pBasis,SitePop,OutputSuffix=""):
    
    if len(SitePop.shape)==1:
        print "'DrawSystem' can't run as only 1 site is given"
        sys.exit()
    elif len(SitePop.shape)==2:
        ci2=RelSitePop(SitePop)
    
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    #     plt.axes().set_aspect('equal', 'datalim')
    ax.scatter(pBasis.rSite[:,0]-pBasis.Lx/2, pBasis.rSite[:,1]-pBasis.Ly/2, pBasis.rSite[:,2]-pBasis.Lz/2, c='b', marker='o',alpha=1)
    scat=ax.scatter(pBasis.rSite[:,0]-pBasis.Lx/2, pBasis.rSite[:,1]-pBasis.Ly/2, pBasis.rSite[:,2]-pBasis.Lz/2, c='r', marker='o',s=5000*ci2[1], alpha=0.25)


    ax.set_xlabel('X')
    ax.set_ylabel('Y')
    ax.set_zlabel('Z')
    
    def animate(i):
        plt.cla()
        ax.scatter(pBasis.rSite[:,0]-pBasis.Lx/2, pBasis.rSite[:,1]-pBasis.Ly/2, pBasis.rSite[:,2]-pBasis.Lz/2, c='b', marker='o')
        scat=ax.scatter(pBasis.rSite[:,0]-pBasis.Lx/2, pBasis.rSite[:,1]-pBasis.Ly/2, pBasis.rSite[:,2]-pBasis.Lz/2, c='r', marker='o', s=5000*ci2[i], alpha=0.25)
        
    
    def init():
        plt.cla()
        ax.scatter(pBasis.rSite[:,0]-pBasis.Lx/2, pBasis.rSite[:,1]-pBasis.Ly/2, pBasis.rSite[:,2]-pBasis.Lz/2, c='b', marker='o')
        scat=ax.scatter(pBasis.rSite[:,0]-pBasis.Lx/2, pBasis.rSite[:,1]-pBasis.Ly/2, pBasis.rSite[:,2]-pBasis.Lz/2, c='r', marker='o', s=5000*ci2[0], alpha=0.25)
        
    
    ani = animation.FuncAnimation(fig, animate, numpy.arange(1, len(SitePop)), init_func=init, interval=500, blit=False,repeat=True)
#     ani = animation.FuncAnimation(fig, animate, numpy.arange(0, SitePop.shape[0]), interval=500, blit=False,repeat=True)
    
    
    Save_Movie=True
    if Save_Movie:
#         Set up formatting for the movie files
#         Writer = animation.writers['ffmpeg']
#         writer = Writer(fps=15, metadata=dict(artist='Me'), bitrate=1800)
        FFwriter = animation.FFMpegWriter()
        ani.save("./SitePop"+OutputSuffix+".mp4",writer = FFwriter, fps=2, extra_args=['-vcodec', 'h264','-pix_fmt', 'yuv420p'])
        plt.close()
#         ani.save('./im.mp4', writer='ffmpeg')
    else:
        plt.show()
    