'''
Created on Oct 22, 2015

@author: cs9114
'''
import numpy

from scipy.optimize import curve_fit
from scipy import stats
import matplotlib.pyplot as plt

def Afunc(t,A,b,c):
    return b*numpy.exp(-A*t)+c

def Plot_EinsteinA(SysPop,tMax,tStep):

    einA,einAerror=Find_EinsteinA(SysPop,tMax,tStep)


    Afig=plt.figure()
    AfigSub=Afig.add_subplot(111)
    AfigSub.set_title('Einstein A-Coefficient')
    AfigSub.scatter(numpy.arange(0,tMax,tStep),SysPop)
    AfigSub.plot(numpy.arange(0,tMax,tStep), numpy.exp(-einA*numpy.arange(0,tMax,tStep)), 'r-')
    AfigSub.text(tMax*0.75,0.9,"A="+str(float('%.3g' % einA))+"$\pm$"+str(float('%.3g' % einAerror)))
    if 0==0:
        AfigSub.set_xlabel("Time a.u.")
    elif 0==1:
        AfigSub.set_xlabel("Time fs.")
    AfigSub.set_ylabel("$|c_{i}|^{2}$")
    AfigSub.axis([0,tMax+tStep,0,1.1])
#     plt.xlim([0,tMax+tStep])
    
    plt.close()
    return (Afig,einA)


def Find_EinsteinA(SysPop,tMax,tStep):
    
    x = numpy.arange(0,tMax,tStep)
    y = numpy.log(SysPop)
#     A = numpy.vstack([x, numpy.ones(len(x))]).T
#     m, c = numpy.linalg.lstsq(A, y)[0]
#     m_error=numpy.mean((SysPop-numpy.exp(m*numpy.arange(0,tMax,tStep)))**2)**0.5
    

    
    m,c,r,p,m_error=stats.linregress(x,y)
    

#     einA, einAvar = curve_fit(Afunc, SysPop, numpy.arange(0,tMax,tStep))
#     einA, einAvar = curve_fit(Afunc, Afunc(numpy.arange(0,tMax,tStep),3), numpy.arange(0,tMax,tStep))
    
#     print einA
    return -m,m_error