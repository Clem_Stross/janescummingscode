'''
Created on Dec 3, 2015

@author: cs9114
'''
import numpy

import matplotlib.pyplot as plt

au2fs=2.418884326505e-2


def Localisation_DM_InvParLength(SiteDM):
    
    InvParLen=numpy.zeros(shape=(len(SiteDM)))
    
    for t in range(0,len(SiteDM),1):
        InvParLen[t]=numpy.sum(SiteDM[t].diagonal())**2/numpy.sum(SiteDM[t].diagonal()**2)
    
    print InvParLen
    
    
def Localisation_InvParLength(SitePop):
    
    if len(SitePop.shape)==1:
        
        return numpy.sum(SitePop)**2/numpy.sum(SitePop**2)
        
    elif len(SitePop.shape)==2:
        InvParLen=numpy.zeros(shape=(len(SitePop)))
        
        for t in range(0,len(SitePop),1):
            if numpy.sum(SitePop[t,:]**2)>0:
                InvParLen[t]=numpy.sum(SitePop[t,:])**2/numpy.sum(SitePop[t,:]**2)
        
        return InvParLen


def Histogram_EigenstateLoc(EigenStates, LHII_Num=1):
    
    EigenLocFig=plt.figure()
    
    if LHII_Num==1:
        EigenLocSubFig=plt.subplot(1,1,1)
    elif LHII_Num>1:
        EigenLocSubFig= plt.subplot(2,1,1)
        EigenLocSubFig_Ring=plt.subplot(2,1,2)
    
    EigenLoc=numpy.array([])
    for State in EigenStates:
        State=State/numpy.linalg.norm(State)
        EigenLoc=numpy.hstack((EigenLoc,Localisation_InvParLength(numpy.absolute(State)**2)))
        
    EigenLocSubFig.hist(EigenLoc, int(len(EigenStates)/2), normed=1, facecolor='green', alpha=0.75)
    EigenLocSubFig.hist(EigenLoc, int(len(EigenStates)/2), normed=1, facecolor='green', alpha=0.75)
    EigenLocSubFig.set_xlabel(r'Eigenvalue delocalisation length $P^{-1}_{BChl}$')
    EigenLocSubFig.set_ylabel("Probability")
    EigenLocSubFig.grid(True)
    EigenLocSubFig.set_xlim([0,len(EigenStates)])
    EigenLocSubFig.set_yticklabels([])
    
    if LHII_Num>1:
        EigenLoc_Ring=numpy.array([])
        for State in EigenStates:
            State=State/numpy.linalg.norm(State)
            State_Ring=State.reshape(LHII_Num,(int(len(State)/LHII_Num)))
            EigenLoc_Ring=numpy.hstack((EigenLoc_Ring,Localisation_InvParLength(numpy.sum(numpy.absolute(State_Ring)**2,axis=1))))
        EigenLocSubFig_Ring.hist(EigenLoc_Ring, int(len(EigenStates)/2), normed=1, facecolor='red', alpha=0.75)
        EigenLocSubFig_Ring.set_xlabel(r'Eigenvalue delocalisation length $P^{-1}_{LHII}$')
        EigenLocSubFig_Ring.set_ylabel("Probability")
        EigenLocSubFig_Ring.grid(True)
        EigenLocSubFig_Ring.set_xlim([0.95,LHII_Num*1.05])
        EigenLocSubFig_Ring.set_yticklabels([])


    
    plt.close()
    return EigenLocFig

def Plot_Localisation_InvParLength(Pop,tMax,tStep,fs=True):
    
        
    if fs==True:
        tMax=tMax*au2fs
        tStep=tStep*au2fs
    
    
    
        
    fig=plt.figure()
    figSub=fig.add_subplot(111)
    figSub.set_title('Inverse Participation')
    
    
    if len(Pop.shape)==2:
        InvParLen=Localisation_InvParLength(Pop)
        figSub.plot(numpy.linspace(0,tMax,len(InvParLen))[1:],InvParLen[1:],color="b", linestyle='-',label="Site")
    elif len(Pop.shape)==3:
        InvParLen1=Localisation_InvParLength(Pop[0])
        InvParLen2=Localisation_InvParLength(Pop[1])
        figSub.plot(numpy.linspace(0,tMax,len(InvParLen1))[1:],InvParLen1[1:],color="b", linestyle='-',label="Site")
        figSub.plot(numpy.linspace(0,tMax,len(InvParLen2))[1:],InvParLen2[1:],color="r", linestyle='-',label="Eigenstate")
        figSub.legend()

    if fs==False:
        figSub.set_xlabel("Time a.u.")
    elif fs==True:
        figSub.set_xlabel("Time fs.")
    figSub.set_ylabel('$P_{BChl}(t)^{-1}$')
    figSub.axis([0,tMax+tStep,0,1.1* Pop.shape[-1]])
    
    plt.close()
    return fig


def Plot_Localisation_InvParLength_Multi(InvParLength,tMax,tStep,fs=True):
        
    if fs==True:
        tMax=tMax*au2fs
        tStep=tStep*au2fs
    
    fig=plt.figure()
    figSub=fig.add_subplot(111)
    figSub.set_title('Inverse Particpiation')
    
    if len(InvParLength.shape)==2:
        figSub.plot(numpy.linspace(0,tMax,InvParLength.shape[1])[1:],numpy.mean(InvParLength, axis=0)[1:],color="b", linestyle='-')
        figSub.plot(numpy.linspace(0,tMax,InvParLength.shape[1])[1:],numpy.mean(InvParLength, axis=0)[1:]+numpy.std(InvParLength, axis=0)[1:],color="b", linestyle=':')
        figSub.plot(numpy.linspace(0,tMax,InvParLength.shape[1])[1:],numpy.mean(InvParLength, axis=0)[1:]-numpy.std(InvParLength, axis=0)[1:],color="b", linestyle=':')
    elif len(InvParLength.shape)==3 and InvParLength.shape[0]==2:
        figSub.plot(numpy.linspace(0,tMax,InvParLength.shape[-1])[1:],numpy.mean(InvParLength[0], axis=0)[1:],color="b", linestyle='-',label="Sites")
        figSub.plot(numpy.linspace(0,tMax,InvParLength.shape[-1])[1:],numpy.mean(InvParLength[0], axis=0)[1:]+numpy.std(InvParLength[0], axis=0)[1:],color="b", linestyle=':')
        figSub.plot(numpy.linspace(0,tMax,InvParLength.shape[-1])[1:],numpy.mean(InvParLength[0], axis=0)[1:]-numpy.std(InvParLength[0], axis=0)[1:],color="b", linestyle=':')
        
        figSub.plot(numpy.linspace(0,tMax,InvParLength.shape[-1])[1:],numpy.mean(InvParLength[1], axis=0)[1:],color="r", linestyle='-',label="Eigenstate")
        figSub.plot(numpy.linspace(0,tMax,InvParLength.shape[-1])[1:],numpy.mean(InvParLength[1], axis=0)[1:]+numpy.std(InvParLength[1], axis=0)[1:],color="r", linestyle=':')
        figSub.plot(numpy.linspace(0,tMax,InvParLength.shape[-1])[1:],numpy.mean(InvParLength[1], axis=0)[1:]-numpy.std(InvParLength[1], axis=0)[1:],color="r", linestyle=':')
        
        figSub.legend()
    
    if fs==False:
        figSub.set_xlabel("Time a.u.")
    elif fs==True:
        figSub.set_xlabel("Time fs.")
    figSub.set_ylabel('$P(t)^{-1}$')
    
    if numpy.amax(InvParLength)>18:
        figSub.axis([0,tMax+tStep,0,28])
    else:
        figSub.axis([0,tMax+tStep,0,19])
    
    plt.close()
    return fig
        