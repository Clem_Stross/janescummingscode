#!/usr/bin/env python2.7
'''
Created on Dec 10, 2015

@author: cs9114
'''
import sys

from Read_InputFile import ReadFromFile

from Pulse_main import Pulse_main
from Pulse_Multi_main import Pulse_Multi_main
from Thermal_main import Thermal_main
from Thermal_Multi_main import Thermal_Multi_main


if __name__ == '__main__':
    if len(sys.argv)==1:
        print("no input file")
        sys.exit()
    elif len(sys.argv)==2 or len(sys.argv)==3:
        
        print "Running"
        InFile=sys.argv[1]
        
        PulseProbWidth=ReadFromFile("PulseProbWidth",InFile,"Float","")
        Temp=ReadFromFile("Temp",InFile,"Float","")
        if PulseProbWidth!="" and Temp!="":
            print "invalid input file"
            sys.exit()
        elif PulseProbWidth=="" and Temp=="":
            print "invalid input file"
            sys.exit()
        
        
        if len(sys.argv)==2:
            if PulseProbWidth!="" and Temp=="":
                Pulse_main(InFile)
            elif PulseProbWidth=="" and Temp!="":
                Thermal_main(InFile)
                
        elif len(sys.argv)==3:
            
            NumOfRuns=int(sys.argv[2])
            if NumOfRuns==0:
                print("NumOfRuns=0")
                sys.exit()
            
            if PulseProbWidth!="" and Temp=="":
                Pulse_Multi_main(InFile,NumOfRuns)
            elif PulseProbWidth=="" and Temp!="":
                Thermal_Multi_main(InFile ,NumOfRuns)
        
    else:
        print "Too many inputs"
        sys.exit()