'''
Created on Mar 7, 2016

@author: cs9114
'''

import numpy
import matplotlib.pyplot as plt

au2fs=2.418884326505e-2



def Measure1(DM):
    Tot=0.0
    for i in range(0,DM.shape[0],1):
        for j in range(i+1,DM.shape[1],1):
            Tot+=(numpy.absolute(DM[i,i])**0.5*numpy.absolute(DM[j,j])**0.5)-numpy.absolute(DM[i,j])
            
    return Tot

def Measure2(DM):
            
    return numpy.absolute(numpy.sum(DM.diagonal()**2)/numpy.trace(numpy.dot(DM,DM)))


def DephasingMeasure(DM):
    assert DM.shape[-1]==DM.shape[-2], "Takes density matrix or list of density matrix"
    
    if len(DM.shape)==2:
        if numpy.trace(DM)>0:
            return Measure2(DM/numpy.trace(DM))
        else:
            return 0
    elif len(DM.shape)==3:
        Output=numpy.zeros(shape=DM.shape[0])
        for i in range(0,DM.shape[0],1):
            if numpy.trace(DM[i])>0:
                Output[i]=Measure2(DM[i]/numpy.trace(DM[i]))
        return Output
        


def DephasingMeasurePlot(DM,tMax,tStep,U=None,fs=True):
#     """Gives a plot of the dephasing in both the given basis and a rotated basis"""
    
    if fs==True:
        tMax=tMax*au2fs
        tStep=tStep*au2fs
     
    assert len(DM.shape)==3
     
    SiteDephase=DephasingMeasure(DM)
     
     
    fig=plt.figure()
    figSub=fig.add_subplot(111)
     
     
    figSub.plot(numpy.linspace(0,tMax,len(SiteDephase))[1:],SiteDephase[1:],label="Site",color="b")
     
     
    if type(U)==numpy.ndarray:
        EigenDephase=numpy.zeros(shape=SiteDephase.shape)
        for i in range(0,len(EigenDephase),1):
            EigenDephase[i]=DephasingMeasure(numpy.dot(numpy.dot(U,DM[i]),numpy.conjugate(numpy.transpose(U))))
        figSub.plot(numpy.linspace(0,tMax,len(EigenDephase))[1:],EigenDephase[1:],label="Eigenstate",color="r")
        figSub.legend()
     
    figSub.axis([0, tMax, 0, 1.1])
    figSub.set_ylabel(r'Dephasing')
    if fs==False:
        figSub.set_xlabel("Time a.u.")
    elif fs==True:
        figSub.set_xlabel("Time fs.")
    
    
    plt.close()
    return fig
    
    
    

def DephasingMeasurePlot_Multi(Data,tMax,tStep,fs=True):
    
    
    if fs==True:
        tMax=tMax*au2fs
        tStep=tStep*au2fs
    
    fig=plt.figure()
    figSub=fig.add_subplot(111)
    figSub.set_title('Dephasing')

    
    if len(Data.shape)==2:
        figSub.plot(numpy.linspace(0,tMax,Data.shape[-1])[1:],numpy.mean(Data, axis=0)[1:],color="b", linestyle='-')
        figSub.plot(numpy.linspace(0,tMax,Data.shape[-1])[1:],numpy.mean(Data, axis=0)[1:]+numpy.std(Data, axis=0)[1:],color="b", linestyle=':')
        figSub.plot(numpy.linspace(0,tMax,Data.shape[-11])[1:],numpy.mean(Data, axis=0)[1:]-numpy.std(Data, axis=0)[1:],color="b", linestyle=':')
    elif len(Data.shape)==3 and Data.shape[0]==2:
        figSub.plot(numpy.linspace(0,tMax,Data.shape[-1])[1:],numpy.mean(Data[0], axis=0)[1:],color="b", linestyle='-',label="Sites")
        figSub.plot(numpy.linspace(0,tMax,Data.shape[-1])[1:],numpy.mean(Data[0], axis=0)[1:]+numpy.std(Data[0], axis=0)[1:],color="b", linestyle=':')
        figSub.plot(numpy.linspace(0,tMax,Data.shape[-1])[1:],numpy.mean(Data[0], axis=0)[1:]-numpy.std(Data[0], axis=0)[1:],color="b", linestyle=':')
        
        figSub.plot(numpy.linspace(0,tMax,Data.shape[-1])[1:],numpy.mean(Data[1], axis=0)[1:],color="r", linestyle='-',label="Eigenstate")
        figSub.plot(numpy.linspace(0,tMax,Data.shape[-1])[1:],numpy.mean(Data[1], axis=0)[1:]+numpy.std(Data[1], axis=0)[1:],color="r", linestyle=':')
        figSub.plot(numpy.linspace(0,tMax,Data.shape[-1])[1:],numpy.mean(Data[1], axis=0)[1:]-numpy.std(Data[1], axis=0)[1:],color="r", linestyle=':')
        
        figSub.legend()
    
    if fs==False:
        figSub.set_xlabel("Time a.u.")
    elif fs==True:
        figSub.set_xlabel("Time fs.")
    figSub.set_ylabel(r'$D(t)$')
    
    figSub.axis([0,tMax+tStep,0,1.1])

    
    plt.close()
    return fig
    SiteDephase=DephasingMeasure(DM)
     
     
    fig=plt.figure()
    figSub=fig.add_subplot(111)
     
     
    figSub.plot(numpy.linspace(0,tMax,len(SiteDephase))[1:],SiteDephase[1:],label="Site",color="b")
     
     
    if type(U)==numpy.ndarray:
        EigenDephase=numpy.zeros(shape=SiteDephase.shape)
        for i in range(0,len(EigenDephase),1):
            EigenDephase[i]=DephasingMeasure(numpy.dot(numpy.dot(U,DM[i]),numpy.conjugate(numpy.transpose(U))))
        figSub.plot(numpy.linspace(0,tMax,len(EigenDephase))[1:],EigenDephase[1:],label="Eigenstate",color="r")
        figSub.legend()
     
    figSub.axis([0, tMax, 0, 1.1])
    figSub.set_ylabel(r'Dephasing')
    if fs==False:
        figSub.set_xlabel("Time a.u.")
    elif fs==True:
        figSub.set_xlabel("Time fs.")
    
    
    plt.close()
    return fig




if __name__ == '__main__':
    n=10
    test=numpy.diag(numpy.random.rand(n))
    test=test/numpy.trace(test)
    print Measure1(test)
    print Measure2(test)
    
        
    Vec=numpy.diag(numpy.random.rand(n))
    test2=numpy.outer(Vec,Vec)
    test2=test2/numpy.trace(test2)
    print Measure1(test2)
    print Measure2(test2)