'''
Created on Oct 26, 2015

@author: cs9114
'''
import numpy
import sys



def ReadFromFile(SearchString,InputPath,Type="Int",Default=None):
    File=open(InputPath,"r")
    Output=numpy.zeros(shape=(0,3))
    Found=0
    
    
    for Line in File.readlines():
        if Line.find("#")>=0:
            Line=Line[0:Line.find("#")]
        if Line.find("%")>=0:
            Line=Line[0:Line.find("%")]
        Cells=Line.replace(',',' ').replace('=',' ').replace('(',' ').replace(')',' ').split()
        if len(Cells)<2:
            continue
        
        if Cells[0]==SearchString:
            if Type in ("float", "Float"):
                File.close()
                return float(Cells[-1])
            elif Type in ("int", "Int"):
                File.close()
                return int(Cells[-1])
            elif Type in ("string", "String"):
                File.close()
                return str(Cells[-1])
            elif Type in ("List", "list"):
                File.close()
                return numpy.array([float(a) for a in Cells[1::]])
            elif Type in ("ListOfList", "listOflist","listoflist","Array","array"):
                Output=numpy.append(Output,numpy.array([[float(a) for a in Cells[1::]]]),axis=0)
            elif Type in ("Bool", "bool"):
                if Cells[1] in ("True", "true"):
                    return True
                if Cells[1] in ("False", "false"):
                    return False
            else:
                print "Type '"+Type+"' not know in 'ReadFromFile'"
                sys.exit()
            Found=1
    
   
    
    if Found==0:
        DefaultGood=True
        if type(Default)==type(None):
                DefaultGood=False
        
        
        if DefaultGood:
                return Default
        else:
            print "SearchString '"+SearchString+"' not found in 'ReadFromFile'"
            sys.exit()
        
    File.close()
    return Output
