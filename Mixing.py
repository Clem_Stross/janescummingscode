'''
Created on Nov 13, 2015

@author: cs9114
'''

import numpy
import scipy

import matplotlib.pyplot as plt

au2fs=2.418884326505e-2


def Mixing_DM(SysDM):
    
    DM_Sys=numpy.zeros(shape=(SysDM.shape[1]+1,SysDM.shape[1]+1),dtype=complex)
    DM_eSys=numpy.zeros(shape=(SysDM.shape[1],SysDM.shape[1]),dtype=complex)
    
    Entropy=numpy.zeros(shape=(SysDM.shape[0]))
    Mixing=numpy.ones(shape=(SysDM.shape[0]))
    Mixing_e=numpy.ones(shape=(SysDM.shape[0]))
    
    
    for t in range(0,SysDM.shape[0],1):
        DM_Sys[0,0]=0
        DM_Sys[1:,1:]=SysDM[t]
        DM_Sys[0,0]=1-numpy.trace(DM_Sys)
        Mixing[t]=numpy.absolute(numpy.trace(numpy.dot(DM_Sys,DM_Sys)))
#         Entropy[t]=-numpy.trace(numpy.dot(DM_Sys,scipy.linalg.logm(DM_Sys)))
        
        
        if DM_Sys[0,0]!=1:
            DM_eSys=SysDM[t]
            DM_eSys=DM_eSys/(1-DM_Sys[0,0])
            
            
            Mixing_e[t]=numpy.absolute(numpy.trace(numpy.dot(DM_eSys,DM_eSys)))
    
    return Mixing,Mixing_e




def Mixing_WF(UnNorm):
    
    Mixing=numpy.ones(shape=(UnNorm.shape[0]))
    Mixing_e=numpy.ones(shape=(UnNorm.shape[0]))
    
    for t in range(0,UnNorm.shape[0],1):
        Holder=numpy.linalg.norm(UnNorm[t])**2
        Mixing[t]=(1-Holder)**2+Holder**2
        
    return Mixing,Mixing_e
        

def Plot_Mixing(SysData,tMax,tStep,fs=True):
    

    
    if fs==True:
        tMax=tMax*au2fs
        tStep=tStep*au2fs
    
    if len(SysData.shape)==3:
        Mixing,Mixing_e=Mixing_DM(SysData)
    elif len(SysData.shape)==2:
        Mixing,Mixing_e=Mixing_WF(SysData)
        
    fig=plt.figure()
    figSub=fig.add_subplot(111)
    figSub.set_title('Mixing')
    figSub.plot(numpy.linspace(0,tMax,len(Mixing))[1:],Mixing[1:],label="Mixing",color="b", linestyle='dashed')
    figSub.plot(numpy.linspace(0,tMax,len(Mixing_e))[1:],Mixing_e[1:],label="eMixing",color="r", linestyle='dashed')
    if fs==False:
        figSub.set_xlabel("Time a.u.")
    elif fs==True:
        figSub.set_xlabel("Time fs.")
    figSub.set_ylabel("tr "+r'$\rho(t)^{2}$')
    figSub.axis([0,tMax+tStep,0,1.1])
    figSub.legend()
    
    plt.close()
    return fig
        

def Plot_Mixing_Multi(Mixing,Mixing_e,tMax,tStep,fs=True):
    

    
    if fs==True:
        tMax=tMax*au2fs
        tStep=tStep*au2fs
    
    fig=plt.figure()
    figSub=fig.add_subplot(111)
    figSub.set_title('Mixing')
    figSub.plot(numpy.linspace(0,tMax,Mixing.shape[1])[1:],numpy.mean(Mixing, axis=0)[1:],label="Mixing",color="b", linestyle='-')
    figSub.plot(numpy.linspace(0,tMax,Mixing.shape[1])[1:],numpy.mean(Mixing, axis=0)[1:]+numpy.std(Mixing, axis=0)[1:],label=None,color="b", linestyle=':')
    figSub.plot(numpy.linspace(0,tMax,Mixing.shape[1])[1:],numpy.mean(Mixing, axis=0)[1:]-numpy.std(Mixing, axis=0)[1:],label=None,color="b", linestyle=':')

    figSub.plot(numpy.linspace(0,tMax,Mixing_e.shape[1])[1:],numpy.mean(Mixing_e, axis=0)[1:],label="e_Mixing",color="r", linestyle='-')
    figSub.plot(numpy.linspace(0,tMax,Mixing_e.shape[1])[1:],numpy.mean(Mixing_e, axis=0)[1:]+numpy.std(Mixing_e, axis=0)[1:],label=None,color="r", linestyle=':')
    figSub.plot(numpy.linspace(0,tMax,Mixing_e.shape[1])[1:],numpy.mean(Mixing_e, axis=0)[1:]-numpy.std(Mixing_e, axis=0)[1:],label=None,color="r", linestyle=':')
#     figSub.errorbar(numpy.linspace(0,tMax,Mixing.shape[1])[1:],numpy.mean(Mixing, axis=0)[1:],yerr=numpy.std(Mixing, axis=0)[1:],label="Mixing",color="b", linestyle='dashed')
#     figSub.errorbar(numpy.linspace(0,tMax,Mixing_e.shape[1])[1:],numpy.mean(Mixing_e, axis=0)[1:],yerr=numpy.std(Mixing_e, axis=0)[1:],label="eMixing",color="r", linestyle='dashed')
    if fs==False:
        figSub.set_xlabel("Time a.u.")
    elif fs==True:
        figSub.set_xlabel("Time fs.")
    figSub.set_ylabel("tr "+r'$\rho(t)^{2}$')
    figSub.axis([0,tMax+tStep,0,1.1])
    figSub.legend()
    
    plt.close()
    return fig
