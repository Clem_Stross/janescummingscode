import numpy
import math

from JaynesCummings_Basis import JaynesCummings_Basis



from Propagate import FormPropagator
from Propagate import PropagateState
from Propagate import PropagateState_WF_Fast

from EinsteinA_Coefficient import Plot_EinsteinA
from EinsteinA_Coefficient import Find_EinsteinA

from PhotonPopulation import PlotPhotonPop
from PhotonPopulation import PlotPhotonPop_Movie

from PhotonPlotter import PlotPhoton_1D
from PhotonPlotter import PlotPhoton_2D

import matplotlib.pyplot as plt


def main():
    
    Esite=30.0
    deltaOmega=30.0
    tMax=1.0
    tStep=0.1

#     pState=Basis(200,205,195) #Define box dimension 
    Lx=200.0
    Ly=200.0
    Lz=1.0
    
    
    pState=JaynesCummings_Basis(Lx,Ly,Lz)
    pState.ReadInSysInfo(numpy.array([[Esite]]),numpy.array([[0,1,0]]),numpy.array([[Lx,Ly,Lz]])/2,False) #Read in system hamiltonian and transition dipole moment
    pState.MakeBasis(Esite, deltaOmega,False,True) #Give \omega and \Delta \omega -> Produce allowed states
    print "nPhot=", pState.nPhot,"nPhotE=", pState.nPhotE 
    
    print pState.rSite
    
    pState.BuildHam_JanesCummings_1p() #build hamiltonian
    
    
    IState=numpy.zeros(shape=(pState.nState)) #InitialState, single excited site
    IState[-1]=1
    
    
    State=PropagateState_WF_Fast(pState.Ham_JC,IState,tMax,tStep,1) #Propergate through time
    
    
    aaa=Plot_EinsteinA(numpy.absolute(State[:,-1])**2,tMax,tStep) #Fit to exp(-A*t). Returns [graph,A]
    aaa[0].savefig('./EinsteinA.pdf')
    
 
    if pState.EPol==True:
        PhotonPop=PlotPhotonPop(numpy.absolute(State[-1,0:pState.nPhotE])**2,numpy.ravel(numpy.column_stack((pState.Omega,pState.Omega))),50,"Cauchy")
          
    elif pState.EPol==False:
        PhotonPop=PlotPhotonPop(numpy.absolute(State[-1,0:pState.nPhotE])**2,pState.Omega,50,"Cauchy")
    PhotonPop.savefig('./PhotonPop.pdf')

    
    if pState.Dim=="1D":
        PlotPhoton_1D(pState,State[:,0:pState.nPhotE])
    elif pState.Dim=="2D":
        PlotPhoton_2D(pState,State[:,0:pState.nPhotE])
    

main()

print "Job Done :-)"

