'''
Created on Nov 9, 2015

@author: cs9114
'''


import sys
import numpy
import random
import math


from JaynesCummings_Basis import JaynesCummings_Basis


from Read_InputFile import ReadFromFile

from LHII_Ham import LHII_Ham


from Propagate import PropagateState_DM_Fast


from PhotonPopulation import PlotPhotonPop

from SitePopulation import Plot_SitePop
from SitePopulation import Plot_RelSitePop
from SitePopulation import Plot_ExpRelSitePop_Thermal

from DrawSystem import DrawSystem


from Mixing import Plot_Mixing

from Localisation import Plot_Localisation_InvParLength
from Localisation import Histogram_EigenstateLoc

from DM_EigenAnal import Plot_DM_NaturalOrbitalAnal
from DM_EigenAnal import Draw_DM_OrbitalAnal

from DephasingMeasure import DephasingMeasurePlot

from PlotDensityMatrix import PlotDensityMatrix
        
        
def Thermal_main(InFile):
        
    OutputSuffix=ReadFromFile("OutputSuffix",InFile,"String","")
    if len(OutputSuffix)>0:
        OutputSuffix="_"+OutputSuffix
        
        
    L=ReadFromFile("L",InFile,"List")
    
    
    Ham=ReadFromFile("Ham",InFile,"List",0)
    Mu=ReadFromFile("Mu",InFile,"List",0)
    Rsite=ReadFromFile("Rsite",InFile,"List",0)
    
    HamType=ReadFromFile("HamType",InFile,"String",0)
    LHII_Num=ReadFromFile("LHII_Num",InFile,"int",1)
    
    #===========================================================================
    # Reading in system information from inputfile
    #===========================================================================
    if type(Ham)!=int and type(Mu)!=int and HamType==0:
        Mu=Mu.reshape(len(Mu)/3,3)
        Ham=Ham.reshape((len(Ham)**0.5,len(Ham)**0.5))
        
        
        if type(Rsite)==numpy.ndarray:
            Rsite=Rsite.reshape(len(Rsite)/3,3)
        else:
            Rsite=numpy.zeros(shape=(0,3))
        for Site in Ham:
            Rsite=numpy.append(Rsite,[L/2],axis=0)
        
    
    
    #===========================================================================
    # Creating system information from pdb file or from ideal structure.
    #===========================================================================
    elif type(Ham)==int and type(Mu)==int and type(HamType)==str:
        Ham,Mu,Rsite=LHII_Ham(InFile)
        Rsite=Rsite+L/2
        
    
    #===========================================================================
    # Read in remaining data from the input file
    #===========================================================================
    
    tMax=ReadFromFile("tMax",InFile,"Float",80)
    tStep=ReadFromFile("tStep",InFile,"Float",0.5)

    
    Omega=ReadFromFile("Omega",InFile,"Float")
    dOmega=ReadFromFile("deltaOmega",InFile,"Float",0.0)
    
    LocBasisShrink=ReadFromFile("LocBasisShrink",InFile,"Bool",False)
    EPol=ReadFromFile("EPol",InFile,"Bool",True)
    k_pm=ReadFromFile("k_pm",InFile,"Bool",True)
    
    
    Temp=ReadFromFile("Temp",InFile,"Float")
    BoxDim=ReadFromFile("AssertBoxDim",InFile,"String","")
    ThermalDirection=ReadFromFile("ThermalDirection",InFile,"String",0)
    Pol=ReadFromFile("Pol",InFile,"List",0)
    
    
    SaveVideo=ReadFromFile("SaveVideo",InFile,"Bool",True)
    StochasticStep=ReadFromFile("StochasticStep",InFile,"int",0)
    if StochasticStep>0 and StochasticStep>int(tMax/tStep):
        print "Invalid StochasticStep" + str(int(tMax/tStep))
        sys.exit()
    if tMax<tStep:
        print "Invalid tMax and tStep"
        sys.exit()
    
    
    #===========================================================================
    # Making Photon States
    #===========================================================================
    
    pState=JaynesCummings_Basis(L[0],L[1],L[2])
    pState.ReadInSysInfo(Ham,Mu,Rsite,LocBasisShrink)
    EigenStateLocFig=Histogram_EigenstateLoc(pState.U,LHII_Num)
    EigenStateLocFig.savefig('./EigenStateLoc'+OutputSuffix+'.pdf')
    
    
    if dOmega==0.0:
        
        MaxE=numpy.amax(numpy.hstack((pState.E_Site,pState.E_k)))-Omega
        MaxMu=numpy.amax(numpy.hstack((pState.mu_Site,pState.mu_k)))
        pState.ReadInBasisInfo(Omega, dOmega, ForceBoxDim=BoxDim, dEMag=MaxE ,MuMag=MaxMu) #Give \omega and \Delta \omega -> Produce allowed states
    elif dOmega>0.0:
        pState.ReadInBasisInfo(Omega, dOmega, ForceBoxDim=BoxDim)
    pState.MakeBasis(EPol,k_pm) 
    print("nPhot=", pState.nPhot,"nPhotE=", pState.nPhotE,"nStep=",int(tMax/tStep), "Dim=", pState.Dim)
    
    pState.BuildHam_JanesCummings_1p(False) #build hamiltonian
    
    
    kCoff=pState.ThermalState(Temp,ThermalDirection,Pol)
    
    
    IState=numpy.zeros(shape=(pState.nState,pState.nState),dtype=complex) #InitialState, single excited site
    for mode in range(0,pState.nPhotE,1):
        IState[mode,mode]=kCoff[mode]
    
    
    
    
    if StochasticStep==0:
        State=PropagateState_DM_Fast(pState.Ham_JC,IState,tMax,tStep,1) #Propergate through time
#    !!!!!!!!!!!!Need to document!!!!!!!!!
    elif StochasticStep>0:
        nStochastic=10
        pState.SetStochasticEnergy(nStochastic,nLHII=LHII_Num)
        
        State=numpy.zeros(shape=(int(tMax/tStep)+1,pState.nState,pState.nState),dtype=complex)
        State[0]=IState
        
        for n in range(0,(int(tMax/tStep)/StochasticStep),1):
            pState.UpdateStochasticSiteEnergy(n*tStep*StochasticStep,nLHII=LHII_Num)
            StochasticIState=State[(n*StochasticStep)]
            State[n*StochasticStep:((n+1)*StochasticStep)+1]=PropagateState_DM_Fast(pState.Ham_JC,StochasticIState,tStep*(StochasticStep+1),tStep,Units=1)
    
    
    
    
    
    
    if pState.EPol==True:
        PhotonPop=PlotPhotonPop(numpy.absolute(State[0].diagonal()[0:pState.nPhotE]),numpy.ravel(numpy.column_stack((pState.Omega,pState.Omega))),25)
    elif pState.EPol==False:
        PhotonPop=PlotPhotonPop(numpy.absolute(State[0].diagonal()[0:pState.nPhotE]),pState.Omega,25)
    PhotonPop.savefig('./PhotonPop'+OutputSuffix+'.pdf')

    
    SitePop=numpy.absolute(numpy.array([State[t].diagonal()[pState.nPhotE:pState.nState] for t in range(0,int(tMax/tStep),1)]))
    EigenPop=numpy.zeros(shape=(SitePop.shape))

    #Needs Testing more
    for t in range(0,EigenPop.shape[0],1):
        EigenDM=numpy.dot(numpy.dot(pState.U,State[t,pState.nPhotE:pState.nState,pState.nPhotE:pState.nState]),numpy.conjugate(numpy.transpose(pState.U))) 
        EigenPop[t]=numpy.absolute(EigenDM.diagonal())
    
#     if SaveVideo==True:
#         DrawSystem(pState,SitePop,OutputSuffix)
    
    
    SitePopFig=Plot_SitePop(SitePop,tMax,tStep,"l")
    SitePopFig.savefig('./SitePop'+OutputSuffix+'.pdf')
    
    EigenPopFig=Plot_SitePop(EigenPop,tMax,tStep,"k")
    EigenPopFig.savefig('./EigenPop'+OutputSuffix+'.pdf')
    
    
    RelSitePopFig=Plot_RelSitePop(SitePop,tMax,tStep,"l")
    RelEigenPopFig=Plot_RelSitePop(EigenPop,tMax,tStep,"k")
    if Temp>0:
        RelEigenPopFig=Plot_ExpRelSitePop_Thermal(pState.E_k,pState.mu_k,tMax,tStep,Temp,"k",RelEigenPopFig)
    RelSitePopFig.savefig('./RelSitePop'+OutputSuffix+'.pdf')
    RelEigenPopFig.savefig('./RelEigenPop'+OutputSuffix+'.pdf')
    
    InvParLengthFig=Plot_Localisation_InvParLength(numpy.array([SitePop,EigenPop]),tMax,tStep)
    InvParLengthFig.savefig('./InvParLength'+OutputSuffix+'.pdf')
    
    
    MixingFig=Plot_Mixing(State[:,pState.nPhotE:pState.nState,pState.nPhotE:pState.nState],tMax,tStep)
    MixingFig.savefig('./Mixing'+OutputSuffix+'.pdf')
    
    
    EigenAnalFig=Plot_DM_NaturalOrbitalAnal(State[:,pState.nPhotE:pState.nState,pState.nPhotE:pState.nState],tMax,tStep,pState)
    EigenAnalFig.savefig('./NatrualOrbital'+OutputSuffix+'.pdf')
    
    DephasingFig=DephasingMeasurePlot(State[:,pState.nPhotE:pState.nState,pState.nPhotE:pState.nState],tMax,tStep,pState.U)
    DephasingFig.savefig('./Dephasing'+OutputSuffix+'.pdf')
    
    if SaveVideo==True:
        Draw_DM_OrbitalAnal(State[:,pState.nPhotE:pState.nState,pState.nPhotE:pState.nState],tMax,tStep,pState,OutputSuffix,"l",NumOfLHII=LHII_Num)
        Draw_DM_OrbitalAnal(State[:,pState.nPhotE:pState.nState,pState.nPhotE:pState.nState],tMax,tStep,pState,OutputSuffix,"k",NumOfLHII=LHII_Num)
        
        PlotDensityMatrix(State[:,pState.nPhotE:pState.nState,pState.nPhotE:pState.nState],tMax,tStep,pState,"l",OutputSuffix)
        PlotDensityMatrix(State[:,pState.nPhotE:pState.nState,pState.nPhotE:pState.nState],tMax,tStep,pState,"k",OutputSuffix)
    
    

    
    


    
    print "Job Done :-)"
    
    
    
    
    
    
    
if __name__ == '__main__':
    if len(sys.argv)==1:
        print("no input file")
        sys.exit()
    elif len(sys.argv)==2:
        Thermal_main(sys.argv[1])
    else:
        print "Too many inputs"
        sys.exit()