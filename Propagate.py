#!/usr/bin/python

import numpy

import scipy.linalg


def FormPropagator(Ham,tStep,Units=1):
    
    if Units==0:
        TimeStep=tStep/(2.418884*(10**-17))
    elif Units==1:
        TimeStep=tStep
    Propagator=scipy.linalg.expm(Ham*-1j*TimeStep)
    return Propagator





def PropagateState(InitalState,Propagator):

    if numpy.linalg.norm(InitalState)<0.99 or numpy.linalg.norm(InitalState)>1.01:
        print "In 'TimePropagation' InitalState not normalised ("+str(numpy.linalg.norm(InitalState))+")"
        InitalState=InitalState/numpy.linalg.norm(InitalState)
    
    NewState=numpy.dot(Propagator,InitalState)
    
    
    return NewState


def PropagateState_WF_Fast(Ham,InitalState,tMax,tStep,Units=0):
    
    State=numpy.zeros(shape=(int(tMax/tStep),len(InitalState)),dtype=complex)
    Prop=FormPropagator(Ham,tStep,Units)
    
    State[0,:]=InitalState[:]
    
    for t in range(1,int(tMax/tStep),1):
        State[t,:]=PropagateState(State[t-1,:],Prop)
        
    return State


def PropagateState_DM_Fast(Ham,InitalState,tMax,tStep,Units=1):
    State=numpy.zeros(shape=(int(tMax/tStep),len(InitalState),len(InitalState)),dtype=complex)
    Prop=FormPropagator(Ham,tStep,Units)
    
    State[0,:,:]=InitalState[:,:]
    
    
    for t in range(1,int(tMax/tStep),1):
        State[t,:,:]=numpy.dot(numpy.dot(Prop,State[t-1,:,:]),numpy.conjugate(numpy.transpose(Prop)))
        
    return State