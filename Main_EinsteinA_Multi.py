# '''
# Created on Oct 22, 2015
# 
# @author: cs9114
# '''

#!/usr/bin/python3

import numpy
import math
import sys
import os


from JaynesCummings_Basis import JaynesCummings_Basis

from Propagate import FormPropagator
from Propagate import PropagateState
from Propagate import PropagateState_WF_Fast

from EinsteinA_Coefficient import Plot_EinsteinA
from EinsteinA_Coefficient import Find_EinsteinA

from Read_InputFile import ReadFromFile

from PhotonPopulation import FitPhotonPop 


if __name__ == '__main__':
    
    tMax=1.0
    tStep=0.1
    
    OutputPath="./Output.txt"
 
#     for n in range(1,25,1):
#         if not os.path.isfile(OutputPath+"_"+str(n)+".txt"):
#             OutputFile=open(OutputPath+"_"+str(n)+".txt","w")
#             break
    OutputFile=open(OutputPath,"w")
    OutputFile.write("Lx \t Ly \t Lz \t E_k \t \delta \omega \t \mu_x \t \mu_y \t \mu_z \t EPol \t k \pm \t \mu \t \pm \t \gamma \t \pm \t A \t \pm \n" )
    
    
    
    InputFiles=sys.argv[1::]
    
    for InFile in InputFiles:
    
        L=ReadFromFile("L",InFile,"List")

        
        
        pState=JaynesCummings_Basis(L[0],L[1],L[2])
        
        
        
        
        E_List=ReadFromFile("ESite",InFile,"List")
        mu_List=ReadFromFile("Mu",InFile,"Array")
        dOmega=ReadFromFile("deltaOmega",InFile,"Float")
        EPol=ReadFromFile("EPol",InFile,"Bool")
        k_pm=ReadFromFile("k_pm",InFile,"Bool")
        
        #Need to add in boolian options
        
        
        for E_Value in E_List:
            
             #Give \omega and \Delta \omega -> Produce allowed states
            
            for mu_Value in mu_List:
                
                pState.MakeBasis(E_Value, dOmega, EPol, k_pm)
                print "E=",E_Value,"\t mu=",mu_Value,"\t nPhot=",pState.nPhot,
     
                pState.ReadInSysInfo(numpy.array([[E_Value]]),numpy.array([mu_Value])) #Read in system hamiltonian and transition dipole moment
                pState.BuildHam_JanesCummings_1p() #build hamiltonian
     
                IState=numpy.zeros(shape=(pState.nState))
                IState[-1]=1
                 
         
                State=PropagateState_WF_Fast(pState.Ham_JC,IState,tMax,tStep,1)
                
                Acoeff,Aerror=Find_EinsteinA(numpy.absolute(State[:,-1])**2,tMax,tStep)
                
                
                if pState.EPol==True:
                    PhotonPop,PhotonPopVar=FitPhotonPop(numpy.absolute(State[-1,0:pState.nPhotE])**2,numpy.ravel(numpy.column_stack((pState.Omega,pState.Omega))),50)
                elif pState.EPol==False:
                    PhotonPop,PhotonPopVar=FitPhotonPop(numpy.absolute(State[-1,0:pState.nPhotE])**2,pState.Omega,50)
                
                print "\t A=",Acoeff
                
                OutputFile.write(str(L[0])+ "\t"+ str(L[1])+ "\t"+ str(L[2])+ "\t"+ str(E_Value)+ "\t"+ str(dOmega)+ "\t"+ str(mu_Value[0])+ "\t"+ str(mu_Value[1]) + "\t"+ str(mu_Value[2])+ "\t"+ str(pState.EPol)+"\t"+str(pState.k_pm)+ "\t")
                OutputFile.write(str(PhotonPop[1])+ "\t"+ str(PhotonPopVar[1,1]**0.5)+"\t"+str(PhotonPop[2])+ "\t"+ str(PhotonPopVar[2,2]**0.5)+ "\t"+ str(Acoeff)+ "\t"+ str(Aerror)+ "\n")
        OutputFile.write("\n")
        print ''
    OutputFile.close()
    print("Job Done :-)")