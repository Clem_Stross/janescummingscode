'''
Created on Nov 3, 2015

@author: cs9114
'''
import numpy
import sys


import matplotlib.pyplot as plt

# Tableau 20 Colors
tableau20 = [(31, 119, 180), (174, 199, 232), (255, 127, 14), (255, 187, 120),  
             (44, 160, 44), (152, 223, 138), (214, 39, 40), (255, 152, 150),  
             (148, 103, 189), (197, 176, 213), (140, 86, 75), (196, 156, 148),  
             (227, 119, 194), (247, 182, 210), (127, 127, 127), (199, 199, 199),  
             (188, 189, 34), (219, 219, 141), (23, 190, 207), (158, 218, 229)]
             
# Tableau Color Blind 10
tableau20blind = [(0, 107, 164), (255, 128, 14), (171, 171, 171), (89, 89, 89),
             (95, 158, 209), (200, 82, 0), (137, 137, 137), (163, 200, 236),
             (255, 188, 121), (207, 207, 207)]
  
# Rescale to values between 0 and 1 
for i in range(len(tableau20)):  
    r, g, b = tableau20[i]  
    tableau20[i] = (r / 255., g / 255., b / 255.)
for i in range(len(tableau20blind)):  
    r, g, b = tableau20blind[i]  
    tableau20blind[i] = (r / 255., g / 255., b / 255.)

# ColorList=('b', 'g', 'r', 'c', 'm', 'y', 'k')
ColorList=tableau20blind
MarkerList=('+', 'x', 'o', 's', 'd')
LineList=([8, 8],[6, 4],[8, 4,4,4],[8, 4, 2, 4, 2, 4],[8, 4, 2, 4])


au2fs=2.418884326505e-2

def Plot_SitePop(SysPop,tMax,tStep,Index="l",fs=True):
    
    if fs==True:
        tMax=tMax*au2fs
        tStep=tStep*au2fs
    
    fig=plt.figure()
    figSub=fig.add_subplot(111)
    
    
    if len(SysPop.shape)==1:
        figSub.scatter(numpy.arange(0,tMax,tStep),SysPop)
    elif len(SysPop.shape)==2:
        for SysN in range(0,SysPop.shape[1],1):
#             figSub.plot(numpy.linspace(0,tMax,len(SysPop)),SysPop[:,SysN],color=ColorList[SysN%len(ColorList)], linestyle='dashed', marker=MarkerList[SysN%len(MarkerList)],label="$"+Index+"$="+str(SysN+1), mfc='none',markeredgecolor=ColorList[SysN%len(ColorList)])
            figSub.plot(numpy.linspace(0,tMax,len(SysPop)),SysPop[:,SysN],color=ColorList[SysN%len(ColorList)], linestyle="-",dashes=LineList[SysN%len(LineList)],label="$"+Index+"$="+str(SysN+1))
    
#     figSub.legend()
    figSub.set_ylabel(r'$\rho_{'+Index+','+Index+'}$')
    if fs==False:
        figSub.set_xlabel("Time a.u.")
    elif fs==True:
        figSub.set_xlabel("Time fs.")
    plt.close()
    return fig


def RelSitePop(SysPop):
    ci2=numpy.ones(shape=SysPop.shape)*float(SysPop.shape[-1])**-1
    
    for t in range(0,SysPop.shape[0],1):
        if numpy.sum(SysPop[t,:])>0.0:
            ci2[t,:]=SysPop[t,:]/numpy.sum(SysPop[t,:])
            
    return ci2

def Plot_RelSitePop(SysPop,tMax,tStep,Index="l",Threshold=0.05,fs=True):
    
    fig=plt.figure()
    figSub=fig.add_subplot(111)
    
    if fs==True:
        tMax=tMax*au2fs
        tStep=tStep*au2fs
    
    if len(SysPop.shape)==1:
        print "'Plot_RelSitePop' can't run as only 1 site is given"
        return fig
    elif len(SysPop.shape)==2:
        
        ci2=RelSitePop(SysPop)
        
        for SysN in range(0,SysPop.shape[1],1):
            if numpy.max(ci2[1:,SysN])>Threshold and SysPop.shape[1]>27:
                aaa=figSub.plot(numpy.linspace(0,tMax,len(ci2))[1:],ci2[1:,SysN],color=ColorList[SysN%len(ColorList)], linestyle="-",dashes=LineList[SysN%len(LineList)],label="$"+Index+"$="+str(SysN+1))
                
#                 figSub.plot(numpy.linspace(0,tMax,len(ci2))[1:],ci2[1:,SysN],color=ColorList[SysN%len(ColorList)], linestyle='dashed', marker=MarkerList[SysN%len(MarkerList)],label="$"+Index+"$="+str(SysN+1), mfc='none',markeredgecolor=ColorList[SysN%len(ColorList)])
            else:
                figSub.plot(numpy.linspace(0,tMax,len(ci2))[1:],ci2[1:,SysN],color=ColorList[SysN%len(ColorList)], linestyle="-",dashes=LineList[SysN%len(LineList)],label='_nolegend_')
#                 figSub.plot(numpy.linspace(0,tMax,len(ci2))[1:],ci2[1:,SysN],color=ColorList[SysN%len(ColorList)], linestyle='dashed', marker=MarkerList[SysN%len(MarkerList)], label=None, mfc='none',markeredgecolor=ColorList[SysN%len(ColorList)])

 
         
#         figSub.legend()
        figSub.axis([0, tMax, 0, 1.1])
        figSub.set_ylabel(r'$\rho^{e}_{'+Index+','+Index+'}$')
        if fs==False:
            figSub.set_xlabel("Time a.u.")
        elif fs==True:
            figSub.set_xlabel("Time fs.")
        plt.close()
        return fig

def Plot_ExpRelSitePop_Pulse(E,Mu,tMax,tStep,E0,dx,Pol,Index="k",Plot=0,fs=True):
    
    #E Sys energy
    #E Sys transition dipole moments
    #E0 laser energy
    #dx laser real width
    #Pol laser polarisation
    #Plot is plot it add data too, if Plot=0 than a new plot is created
    
    if fs==True:
        tMax=tMax*au2fs
        tStep=tStep*au2fs
    
    
    nSys=len(E)
    
    if E.shape!=(nSys,) and Mu.shape!=(nSys,3):
        print "Shapes wrong in Plot_ExpRelSitePop_Pulse"
        
    if Plot==0:
        fig=plt.figure()
        figSub=fig.add_subplot(111)
    else:
        fig=Plot
        figSub=fig.add_subplot(111)
    
    ci2=numpy.zeros(shape=(nSys))
    ci2_Mu=numpy.zeros(shape=(nSys))
    
    c=137.0
    dk=1/dx
    dE=c*dk
    
    
        
    
    for State in range(0,nSys,1):
#         ci2[State]=(numpy.dot(Pol,Mu[State])**2)*numpy.exp(-(E[State]-E0)**2/(dE**2))
        ci2[State]=(numpy.dot(Pol,Mu[State])**2)*numpy.exp(-((E[State]-E0)**2)*2*(dx**2)/(c**2))
        ci2_Mu[State]=(numpy.linalg.norm(Mu[State])**2)
    if numpy.sum(ci2)!=0:
        ci2=ci2/numpy.sum(ci2)
        ci2_Mu=ci2_Mu/numpy.sum(ci2_Mu)
    
    
    for State in range(0,nSys,1):
        figSub.plot(numpy.array([tMax*15/20.0,tMax*21/20]),numpy.array([ci2[State],ci2[State]]),color=ColorList[State%len(ColorList)])
        figSub.plot(numpy.array([-tMax/20.0,tMax*5/20]),numpy.array([ci2_Mu[State],ci2_Mu[State]]),color=ColorList[State%len(ColorList)])


    figSub.axis([-tMax/20.0, tMax*21/20, 0, 1.1])
    figSub.axvline(x=0,color="k")
    figSub.axvline(x=tMax,color="k")

    
    figSub.set_ylabel(r'$\rho^{e}_{'+Index+','+Index+'}$')
    if fs==False:
        figSub.set_xlabel("Time a.u.")
    elif fs==True:
        figSub.set_xlabel("Time fs.")
    plt.close()
    return fig
    


def Plot_ExpRelSitePop_Thermal(E,Mu,tMax,tStep,Temp,Index="k",Plot=0,fs=True):
    
    #E Sys energy
    #E Sys transition dipole moments
    #Plot is plot it add data too, if Plot=0 than a new plot is created
    
    
    if fs==True:
        tMax=tMax*au2fs
        tStep=tStep*au2fs
    
    nSys=len(E)
    
    if E.shape!=(nSys,) and Mu.shape!=(nSys,3):
        print "Shapes wrong in Plot_ExpRelSitePop_Pulse"
        
    if Plot==0:
        fig=plt.figure()
        figSub=fig.add_subplot(111)
    else:
        fig=Plot
        figSub=fig.add_subplot(111)
    
    ci2_Bolt=numpy.zeros(shape=(nSys))
    ci2_Mu=numpy.zeros(shape=(nSys))
    
    kb=(1.3806*(10**-23))/(4.35974417*(10**-18))
    

    
    for State in range(0,nSys,1):
        ci2_Bolt[State]=numpy.exp(-E[State]/(kb*Temp))
        ci2_Mu[State]=(numpy.linalg.norm(Mu[State])**2)
    if numpy.sum(ci2_Bolt)!=0:
        ci2_Bolt=ci2_Bolt/numpy.sum(ci2_Bolt)
        ci2_Mu=ci2_Mu/numpy.sum(ci2_Mu)
    
    
    for State in range(0,nSys,1):
        figSub.plot(numpy.array([tMax*15/20.0,tMax*21/20]),numpy.array([ci2_Bolt[State],ci2_Bolt[State]]),color=ColorList[State%len(ColorList)])
        figSub.plot(numpy.array([-tMax/20.0,tMax*5/20]),numpy.array([ci2_Mu[State],ci2_Mu[State]]),color=ColorList[State%len(ColorList)])


    figSub.axis([-tMax/20.0, tMax*21/20, 0, 1.1])
    figSub.axvline(x=0,color="k")
    figSub.axvline(x=tMax,color="k")
#     box = figSub.get_position()
#     figSub.set_position([box.x0, box.y0, box.width * 0.95, box.height])
    
    figSub.set_ylabel(r'$\rho^{e}_{'+Index+','+Index+'}$')
    if fs==False:
        figSub.set_xlabel("Time a.u.")
    elif fs==True:
        figSub.set_xlabel("Time fs.")

    plt.close()
    return fig