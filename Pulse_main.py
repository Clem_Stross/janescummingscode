'''
Created on Nov 3, 2015

@author: cs9114
'''

import sys
import numpy

import math

from JaynesCummings_Basis import JaynesCummings_Basis


from Read_InputFile import ReadFromFile

from LHII_Ham import LHII_Ham

from Propagate import PropagateState_WF_Fast


from PhotonPopulation import PlotPhotonPop

from SitePopulation import Plot_SitePop
from SitePopulation import Plot_RelSitePop
from SitePopulation import Plot_ExpRelSitePop_Pulse

from Mixing import Plot_Mixing

from PhotonPlotter import PlotPhoton_1D
from PhotonPlotter import PlotPhoton_2D
from PhotonPlotter import PlotPhoton_3D

from DrawSystem import DrawSystem

from Localisation import Plot_Localisation_InvParLength
from Localisation import Histogram_EigenstateLoc


from DM_EigenAnal import Draw_WF_OrbitalAnal


import matplotlib.pyplot as plt


        
def Pulse_main(InFile):
        
    OutputSuffix=ReadFromFile("OutputSuffix",InFile,"String","")
    if len(OutputSuffix)>0:
        OutputSuffix="_"+OutputSuffix
    
    L=ReadFromFile("L",InFile,"List")
    
    
    Ham=ReadFromFile("Ham",InFile,"List",0)
    Mu=ReadFromFile("Mu",InFile,"List",0)
    Rsite=ReadFromFile("Rsite",InFile,"List",0)
    
    HamType=ReadFromFile("HamType",InFile,"String",0)
    
    #===========================================================================
    # Reading in system information from inputfile
    #===========================================================================
    if type(Ham)!=int and type(Mu)!=int and HamType==0:
        Mu=Mu.reshape(len(Mu)/3,3)
        Ham=Ham.reshape((len(Ham)**0.5,len(Ham)**0.5))
        
        
        if type(Rsite)==numpy.ndarray:
            Rsite=Rsite.reshape(len(Rsite)/3,3)
        else:
            Rsite=numpy.zeros(shape=(0,3))
        for Site in Ham:
            Rsite=numpy.append(Rsite,[L/2],axis=0)
        
    
    
    #===========================================================================
    # Creating system information from pdb file or from ideal structure.
    #===========================================================================
    elif type(Ham)==int and type(Mu)==int and type(HamType)==str:
        Ham,Mu,Rsite=LHII_Ham(InFile)
        Rsite=Rsite+L/2
    
    
    
    #===========================================================================
    # Read in remaining data from the input file
    #===========================================================================
    
    
    LocBasisShrink=ReadFromFile("LocBasisShrink",InFile,"Bool",False)
    EPol=ReadFromFile("EPol",InFile,"Bool",True)
    k_pm=ReadFromFile("k_pm",InFile,"Bool",True)
    PulsePol=ReadFromFile("PulsePol",InFile,"List",0)
    if type(PulsePol)!=numpy.ndarray:
            PulsePol=numpy.array([0,1,0])
    else:
        PulsePol=PulsePol/numpy.linalg.norm(PulsePol)
            
    
    Omega=ReadFromFile("Omega",InFile,"Float")
    BoxDim=ReadFromFile("AssertBoxDim",InFile,"String","")
    PulseProbWidth=ReadFromFile("PulseProbWidth",InFile,"Float")
    dOmega=ReadFromFile("deltaOmega",InFile,"Float",0.0)
    
    
    tMax=ReadFromFile("tMax",InFile,"Float",0)
    tStep=ReadFromFile("tStep",InFile,"Float",0.05)
    
    if tMax==0:
        tMax=(PulseProbWidth*9)/137
    
    SaveVideo=ReadFromFile("SaveVideo",InFile,"Bool",True)
    LHII_Num=ReadFromFile("LHII_Num",InFile,"int",1)
    
    
    
    #===========================================================================
    # Start set up and calculation 
    #===========================================================================
    
    pState=JaynesCummings_Basis(L[0],L[1],L[2])
    pState.ReadInSysInfo(Ham,Mu,Rsite,LocBasisShrink)
    EigenStateLocFig=Histogram_EigenstateLoc(pState.U,LHII_Num)
    EigenStateLocFig.savefig('./EigenStateLoc'+OutputSuffix+'.pdf')
    
    
    if dOmega==0.0:
        
        MaxE=numpy.amax(numpy.hstack((pState.E_Site,pState.E_k)))-Omega
        MaxE=max([MaxE,4*137*(2/PulseProbWidth)])
        MaxMu=numpy.amax(numpy.hstack((pState.mu_Site,pState.mu_k)))
        pState.ReadInBasisInfo(Omega, dOmega, ForceBoxDim=BoxDim, dEMag=MaxE ,MuMag=MaxMu) #Give \omega and \Delta \omega -> Produce allowed states
    elif dOmega>0.0:
        pState.ReadInBasisInfo(Omega, dOmega, ForceBoxDim=BoxDim)
    pState.MakeBasis(EPol,k_pm) 
    print("nPhot=", pState.nPhot,"nPhotE=", pState.nPhotE,"nStep=",int(tMax/tStep), "Dim=", pState.Dim)
    
    pState.BuildHam_JanesCummings_1p(False) #build hamiltonian
    
    
    if pState.Dim=="1D":
        kCoff=pState.Pulse_1D_kSpace(Omega/137,PulseProbWidth,(L[0]/2)-(3*PulseProbWidth),PulsePol)
    else:
        print "Not looking a 2D or 3D"
        sys.exit()
#     elif pState.Dim=="2D":
#         kCoff=pState.Pulse_2D_kSpace(Omega/137,numpy.array([1.0,0.0,0.0]),PulseProbWidth,PulseProbWidth,pState.L/2-(numpy.array([1,0.0,0.0])*3*PulseProbWidth),numpy.array([0,1,0]))
#     elif pState.Dim=="3D":
#         kCoff=pState.Pulse_3D_kSpace(Omega/137,numpy.array([1.0,0.0,0.0]),PulseProbWidth,PulseProbWidth,pState.L/2-(numpy.array([1,0.0,0.0])*3*PulseProbWidth),numpy.array([0,1,0]))
    
    
    IState=numpy.zeros(shape=(pState.nState),dtype=complex) #InitialState, single excited site
    IState[0:pState.nPhotE]=kCoff
    
    
    State=PropagateState_WF_Fast(pState.Ham_JC,IState,tMax,tStep,1) #Propergate through time
    
    #Might want to add logic here
    SitePop=numpy.absolute(State[:,pState.nPhotE:pState.nState])**2
    EigenPop=numpy.zeros(shape=(SitePop.shape))
    
    #Needs Testing more
    for t in range(0,EigenPop.shape[0],1):
        EigenPop[t]=numpy.absolute(numpy.dot(pState.U,State[t,pState.nPhotE:pState.nState]))**2

        
    if pState.EPol==True:
        PhotonPop=PlotPhotonPop(numpy.absolute(State[1,0:pState.nPhotE])**2,numpy.ravel(numpy.column_stack((pState.Omega,pState.Omega))),25,"Gaussian")
    elif pState.EPol==False:
        PhotonPop=PlotPhotonPop(numpy.absolute(State[1,0:pState.nPhotE])**2,pState.Omega,25,"Gaussian")
    PhotonPop.savefig('./PhotonPop'+OutputSuffix+'.pdf')
    
    
    if SaveVideo==True:
#         DrawSystem(pState,SitePop,OutputSuffix)
    
        if pState.Dim=="1D":
            PlotPhoton_1D(pState,State[:,0:pState.nPhotE],OutputSuffix)
        elif pState.Dim=="2D":
            PlotPhoton_2D(pState,State[:,0:pState.nPhotE],OutputSuffix,25)
        elif pState.Dim=="3D":
            PlotPhoton_3D(pState,State[:,0:pState.nPhotE],OutputSuffix,15)
        
    
    SitePopFig=Plot_SitePop(SitePop,tMax,tStep,"l")
    SitePopFig.savefig('./SitePop'+OutputSuffix+'.pdf')
    
    EigenPopFig=Plot_SitePop(EigenPop,tMax,tStep,"k")
    EigenPopFig.savefig('./EigenPop'+OutputSuffix+'.pdf')
    
    
    RelSitePopFig=Plot_RelSitePop(SitePop,tMax,tStep,"l")
    RelEigenPopFig=Plot_RelSitePop(EigenPop,tMax,tStep,"k")
    Plot_ExpRelSitePop_Pulse(pState.E_k,pState.mu_k,tMax,tStep,Omega,PulseProbWidth,PulsePol,"k",RelEigenPopFig)
    
    RelSitePopFig.savefig('./RelSitePop'+OutputSuffix+'.pdf')
    RelEigenPopFig.savefig('./RelEigenPop'+OutputSuffix+'.pdf')
    
    
    
    MixingFig=Plot_Mixing(State[:,pState.nPhotE:pState.nState],tMax,tStep)
    MixingFig.savefig('./Mixing'+OutputSuffix+'.pdf')
    
    InvParLengthFig=Plot_Localisation_InvParLength(SitePop,tMax,tStep)
    InvParLengthFig.savefig('./InvParLength'+OutputSuffix+'.pdf')
    
    if SaveVideo==True:
        Draw_WF_OrbitalAnal(State[:,pState.nPhotE:pState.nState],tMax,tStep,pState,OutputSuffix,"k",NumOfLHII=LHII_Num,Theshold=1*10**-2)
    
    
    print("Job Done :-)")
    
    
if __name__ == '__main__':
    if len(sys.argv)==1:
        print("no input file")
        sys.exit()
    elif len(sys.argv)==2:
        Pulse_main(sys.argv[1])
    else:
        print "Too many inputs"
        sys.exit()