'''
Created on Dec 4, 2015

@author: cs9114
'''

import sys
import numpy

import math

from JaynesCummings_Basis import JaynesCummings_Basis


from Read_InputFile import ReadFromFile

from LHII_Ham import LHII_Ham

from Propagate import PropagateState_DM_Fast


from Mixing import Mixing_DM
from Mixing import Plot_Mixing_Multi


from Localisation import Localisation_InvParLength
from Localisation import Plot_Localisation_InvParLength_Multi

from DephasingMeasure import DephasingMeasure
from DephasingMeasure import DephasingMeasurePlot_Multi

from progressbar import *

import matplotlib.pyplot as plt


def Thermal_Multi_main(InFile,NumOfRuns):
        
    OutputSuffix=ReadFromFile("OutputSuffix",InFile,"String","")
    if len(OutputSuffix)>0:
        OutputSuffix="_"+OutputSuffix
            
            
    
    L=ReadFromFile("L",InFile,"List")
    
    
    LocBasisShrink=ReadFromFile("LocBasisShrink",InFile,"Bool",False)
    EPol=ReadFromFile("EPol",InFile,"Bool",True)
    k_pm=ReadFromFile("k_pm",InFile,"Bool",True)
    PulsePol=ReadFromFile("PulsePol",InFile,"List",0)
    if type(PulsePol)!=numpy.ndarray:
            PulsePol=numpy.array([0,1,0])
    else:
        PulsePol=PulsePol/numpy.linalg.norm(PulsePol)
    
    
    tMax=ReadFromFile("tMax",InFile,"Float",80)
    tStep=ReadFromFile("tStep",InFile,"Float",2)

    
    Omega=ReadFromFile("Omega",InFile,"Float")
    dOmega=ReadFromFile("deltaOmega",InFile,"Float")
    
    LocBasisShrink=ReadFromFile("LocBasisShrink",InFile,"Bool",True)
    EPol=ReadFromFile("EPol",InFile,"Bool",True)
    k_pm=ReadFromFile("k_pm",InFile,"Bool",True)
    
    
    Temp=ReadFromFile("Temp",InFile,"Float")
    BoxDim=ReadFromFile("AssertBoxDim",InFile,"String","")

    
    
    
    #===========================================================================
    # Making Photon States
    #===========================================================================
    
    pState=JaynesCummings_Basis(L[0],L[1],L[2])
    pState.ReadInBasisInfo(Omega, dOmega, ForceBoxDim=BoxDim)
    pState.MakeBasis(EPol,k_pm) 
    print("nPhot=", pState.nPhot,"nPhotE=", pState.nPhotE,"nStep=",int(tMax/tStep))

    
    
    kCoff=pState.ThermalState(Temp)
    
    
    widgets = ['Thermal Multi Run: ', Percentage(), ' ', Bar(marker='0',left='[',right=']'),' ', ETA(), ' ', FileTransferSpeed()] #see docs for other options
    pbar = ProgressBar(widgets=widgets, maxval=NumOfRuns)
    pbar.start()
    
    
    
    Mixing_All=numpy.zeros(shape=(NumOfRuns,int(tMax/tStep)))
    Mixing_e_All=numpy.zeros(shape=(NumOfRuns,int(tMax/tStep)))
    InvParLength_All=numpy.zeros(shape=(NumOfRuns,int(tMax/tStep)))
    InvParLength_Eigen_All=numpy.zeros(shape=(NumOfRuns,int(tMax/tStep)))
    SiteDephase_All=numpy.zeros(shape=(NumOfRuns,int(tMax/tStep)))
    EigenDephase_All=numpy.zeros(shape=(NumOfRuns,int(tMax/tStep)))
    
    for Run in range(0,NumOfRuns):
        
        Ham,Mu,Rsite=LHII_Ham(InFile)
        Rsite=Rsite+L/2
        
        pState.ReadInSysInfo(Ham,Mu,Rsite,LocBasisShrink)
        pState.BuildHam_JanesCummings_1p(False) #build hamiltonian
        
        IState=numpy.zeros(shape=(pState.nState,pState.nState),dtype=complex) #InitialState, single excited site
        for mode in range(0,pState.nPhotE,1):
            IState[mode,mode]=kCoff[mode]
        
        State=PropagateState_DM_Fast(pState.Ham_JC,IState,tMax,tStep,1) #Propergate through time
    
        #Might want to add logic here
        
        SitePop=numpy.absolute(numpy.array([State[t].diagonal()[pState.nPhotE:pState.nState] for t in range(0,int(tMax/tStep),1)]))
        EigenPop=numpy.zeros(shape=SitePop.shape)
        
        SiteDephase=DephasingMeasure(State[:,pState.nPhotE:pState.nState,pState.nPhotE:pState.nState])
        EigenDepahse=numpy.zeros(shape=SiteDephase.shape)
        for t in range(0,EigenDepahse.shape[0],1):
            EigenDM=numpy.dot(numpy.dot(pState.U,State[t,pState.nPhotE:pState.nState,pState.nPhotE:pState.nState]),numpy.conjugate(numpy.transpose(pState.U)))
            EigenPop[t]=numpy.absolute(EigenDM.diagonal())
            EigenDepahse[t]=DephasingMeasure(EigenDM)
        
        
        
        Mixing,Mixing_e=Mixing_DM(State[:,pState.nPhotE:pState.nState,pState.nPhotE:pState.nState])
        InvParLength=Localisation_InvParLength(SitePop)
        InvParLength_Eigen=Localisation_InvParLength(EigenPop)
        
        Mixing_All[Run,:]=Mixing
        Mixing_e_All[Run,:]=Mixing_e
        InvParLength_All[Run,:]=InvParLength
        InvParLength_Eigen_All[Run,:]=InvParLength_Eigen
        SiteDephase_All[Run,:]=SiteDephase
        EigenDephase_All[Run,:]=EigenDepahse
        
        pbar.update(Run)
    pbar.finish()
    
    
    MixingFig_Multi=Plot_Mixing_Multi(Mixing_All,Mixing_e_All,tMax,tStep)
    MixingFig_Multi.savefig('./Mixing_Multi'+OutputSuffix+'.pdf')
    
    
    InvParLengthFig_Multi=Plot_Localisation_InvParLength_Multi(numpy.array([InvParLength_All,InvParLength_Eigen_All]),tMax,tStep)
    InvParLengthFig_Multi.savefig('./InvParLength_Multi'+OutputSuffix+'.pdf')
    
    DephasingFig_Multi=DephasingMeasurePlot_Multi(numpy.array([SiteDephase_All,EigenDephase_All]),tMax,tStep)
    DephasingFig_Multi.savefig('./Dephasing_Multi'+OutputSuffix+'.pdf')
    
    print "Job Done :-)"
        
        
        
        
if __name__ == '__main__':
    if len(sys.argv)==1:
        print("no input file")
        sys.exit()
    elif len(sys.argv)==2:
        print("File and NumOfRuns needed")
        sys.exit()
        
    elif len(sys.argv)==3:
        NumOfRuns=int(sys.argv[2])
        if NumOfRuns==0:
            print("NumOfRuns=0")
            sys.exit()
        else:
            Thermal_Multi_main(sys.argv[1],NumOfRuns)
    else:
        print "Too many inputs"
