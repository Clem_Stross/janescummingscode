'''
Created on Oct 27, 2015

@author: cs9114
'''

from JaynesCummings_Basis import JaynesCummings_Basis

import math
import numpy

import matplotlib.pyplot as plt
import matplotlib.animation as animation

from mpl_toolkits.mplot3d import axes3d
from matplotlib import cm

plt.rcParams['animation.ffmpeg_path'] = '/opt/local/bin/ffmpeg'

# Save_Movie=False
Save_Movie=True

def PlotPhoton_1D(pBasis,PhotoCoeff,OutputSuffix="",QuadNum=500):
    
    
    fig, ax = plt.subplots()
    
    x=numpy.linspace(0,pBasis.Lx,QuadNum)        # x-array
    line, = ax.plot(x, numpy.zeros(shape=(x.shape)))
    
    y1=numpy.zeros(shape=(x.shape),dtype=complex)
    if pBasis.EPol==True:
        for mode in range(0,pBasis.nPhotE,1):
            y1+=numpy.exp(-1j*x*pBasis.kVec[math.floor(mode/2),0])*PhotoCoeff[1,mode]
    elif pBasis.EPol==False:
        for mode in range(0,pBasis.nPhot,1):
            y1+=numpy.exp(-1j*x*pBasis.kVec[mode,0])*PhotoCoeff[1,mode]
    
    
    plt.xlim(0,pBasis.Lx)
    plt.ylim(0,1.5*numpy.amax(numpy.absolute(y1)**2))
    
    def animate(i):
        y=numpy.zeros(shape=(x.shape),dtype=complex)
        if pBasis.EPol==True:
            for mode in range(0,pBasis.nPhotE,1):
                y+=numpy.exp(-1j*x*pBasis.kVec[math.floor(mode/2),0])*PhotoCoeff[i,mode]
#                 y+=numpy.sin(x*pBasis.kVec[math.floor(mode/2),0])*PhotoCoeff[i,mode]
        elif pBasis.EPol==False:
            for mode in range(0,pBasis.nPhot,1):
                y+=numpy.exp(-1j*x*pBasis.kVec[mode,0])*PhotoCoeff[i,mode]
#                 y+=numpy.sin(x*pBasis.kVec[math.floor(mode/2),0])*PhotoCoeff[i,mode]
        
        line.set_ydata(numpy.absolute(y)**2)  # update the data
        return line,
    
    #Init only required for blitting to give a clean slate.
    def init():
        line.set_ydata(numpy.ma.array(x, mask=True))
        return line,
    

    
    ani = animation.FuncAnimation(fig, animate, numpy.arange(0, len(PhotoCoeff)), init_func=init, interval=500, blit=False,repeat=True)
    

    if Save_Movie:
#         Set up formatting for the movie files
#         Writer = animation.writers['ffmpeg']
#         writer = Writer(fps=15, metadata=dict(artist='Me'), bitrate=1800)
        FFwriter = animation.FFMpegWriter()
        ani.save("./Photon"+OutputSuffix+".mp4",writer = FFwriter, fps=2, extra_args=['-vcodec', 'h264','-pix_fmt', 'yuv420p'])
#         ani.save('./im.mp4', writer='ffmpeg')
        plt.close()
    else:
        plt.show()






def PlotPhoton_2D(pBasis,PhotoCoeff,OutputSuffix="",QuadNum=500):
    
    
    def forceAspect(ax,aspect=1):
        im = ax.get_images()
        extent =  im[0].get_extent()
        ax.set_aspect(abs((extent[1]-extent[0])/(extent[3]-extent[2]))/aspect)
    
    
    
    PhotonFig=plt.figure()
    PhotonSubFig=PhotonFig.add_subplot(1,1,1)
     
    plt.axes().set_aspect('equal', 'datalim')
    
     
    x=numpy.linspace(0,pBasis.Lx, QuadNum)        # x-array
    y=numpy.linspace(0,pBasis.Ly, QuadNum)
     
    X,Y = numpy.meshgrid(x,y)
    Z=numpy.zeros(shape=(X.shape))
    
    


    MaxVal=numpy.zeros(shape=(X.shape),dtype=complex)
    if pBasis.EPol==True:
        for mode in range(0,pBasis.nPhotE,1):
            MaxVal+=numpy.exp(1j*((X*pBasis.kVec[math.floor(mode/2),0])+(Y*pBasis.kVec[math.floor(mode/2),1])))*PhotoCoeff[1,mode]
    elif pBasis.EPol==False:
        for mode in range(0,pBasis.nPhot,1):
            MaxVal+=numpy.exp(1j*(X*pBasis.kVec[mode,0]+Y*pBasis.kVec[mode,1]))*PhotoCoeff[1,mode]
    
    
    quad=plt.pcolormesh(X,Y,Z,vmin=0, vmax=numpy.amax(numpy.absolute(MaxVal))*1.5,cmap="gray_r")
#     quad=plt.pcolormesh(X,Y,Z,vmin=0, vmax=20.0,cmap="gray_r")
    plt.axis([0, pBasis.Lx, 0, pBasis.Ly])
    

    
#     for Site in range(0,pBasis.nSys,1):
#         circle=plt.Circle((pBasis.rSite[Site,0:1]),20,color='b',fill=False)
#         PhotonSubFig.add_artist(circle)

#     for Site in range(0,pBasis.nSys,1):
#         circle = plt.Circle(pBasis.rSite[Site,0:1], radius=0.75, fc='y')
#         PhotonSubFig.add_patch(circle)
#     plt.plot(pBasis.rSite[:,1],pBasis.rSite[:,2],"g",markersize=1000.0)
    

    def animate(i):
        PhotProb=numpy.zeros(shape=(X.shape),dtype=complex)
        if pBasis.EPol==True:
            for mode in range(0,pBasis.nPhotE,1):
                PhotProb+=numpy.exp(1j*((X*pBasis.kVec[math.floor(mode/2),0])+(Y*pBasis.kVec[math.floor(mode/2),1])))*PhotoCoeff[i,mode]
        elif pBasis.EPol==False:
            for mode in range(0,pBasis.nPhot,1):
                PhotProb+=numpy.exp(1j*(X*pBasis.kVec[mode,0]+Y*pBasis.kVec[mode,1]))*PhotoCoeff[i,mode]
                
        Z=(numpy.absolute(PhotProb)**2)[:-1,:-1]
        quad.set_array(Z.ravel()) # update the data
        

#         plt.pcolormesh(X,Y,numpy.absolute(PhotProb)**2,vmin=0, vmax=10) # update the data
#         return ax,
#     
#     
    def init():
        quad.set_array(numpy.zeros(shape=(X.shape)).ravel())
#         plt.pcolormesh(X,Y,numpy.zeros(shape=(X.shape)),vmin=0, vmax=10)
#         return ax,

    plt.colorbar()
    ani = animation.FuncAnimation(PhotonFig, animate, numpy.arange(0, len(PhotoCoeff)), init_func=init, interval=500, blit=False,repeat=True)

    
    if Save_Movie:
#         Set up formatting for the movie files
#         Writer = animation.writers['ffmpeg']
#         writer = Writer(fps=15, metadata=dict(artist='Me'), bitrate=1800)
        FFwriter = animation.FFMpegWriter()
        ani.save("./Photon"+OutputSuffix+".mp4",writer = FFwriter, fps=2, extra_args=['-vcodec', 'h264','-pix_fmt', 'yuv420p'])
#         ani.save('./im.mp4', writer='ffmpeg')
    else:
        plt.show()



def PlotPhoton_3D(pBasis,PhotoCoeff,OutputSuffix="",QuadNum=100):
    
    def forceAspect(ax,aspect=1):
        im = ax.get_images()
        extent =  im[0].get_extent()
        ax.set_aspect(abs((extent[1]-extent[0])/(extent[3]-extent[2]))/aspect)
    
    
    
     

    
     
    x=numpy.linspace(0,pBasis.Lx, QuadNum)        # x-array
    y=numpy.linspace(0,pBasis.Ly, QuadNum)
    z=numpy.linspace(0,pBasis.Lz, QuadNum)
    
    X,Y,Z = numpy.meshgrid(x,y,z)
    
    
    def GetPhotonProb3D(i):
        PhotAmp=numpy.zeros(shape=(X.shape),dtype=complex)
        if pBasis.EPol==True:
            for mode in range(0,pBasis.nPhotE,1):
                PhotAmp+=numpy.exp(((-1)**mode%2)*1j*((X*pBasis.kVec[math.floor(mode/2),0])+(Y*pBasis.kVec[math.floor(mode/2),1])+(Z*pBasis.kVec[math.floor(mode/2),2])))*PhotoCoeff[i,mode]
        elif pBasis.EPol==False:
            for mode in range(0,pBasis.nPhot,1):
                PhotAmp+=numpy.exp(1j*(X*pBasis.kVec[mode,0]+Y*pBasis.kVec[mode,1]+Z*pBasis.kVec[mode,2]))*PhotoCoeff[i,mode]
                
        PhotProb=(numpy.absolute(PhotAmp)**2)#[:-1,:-1]
        
        return numpy.sum(PhotProb,axis=1),numpy.sum(PhotProb,axis=0),numpy.sum(PhotProb,axis=2)
#         return numpy.sum(PhotProb,axis=0),numpy.sum(PhotProb,axis=1),numpy.sum(PhotProb,axis=2)
    
    
    fig = plt.figure()
    plt.axes().set_aspect('equal', 'datalim')
    ax = fig.gca(projection='3d')
    
    ppZY,ppXZ,ppXY=GetPhotonProb3D(0)
    
    
    
    
    def animate(i):
        
        ax.cla()
        ax.set_xlabel('X')
        ax.set_xlim(0, pBasis.Lx)
        ax.set_ylabel('Y')
        ax.set_ylim(0, pBasis.Ly)
        ax.set_zlabel('Z')
        ax.set_zlim(0, pBasis.Lz)
        
        
        ppZY,ppXZ,ppXY=GetPhotonProb3D(i)
        csetX = ax.contour(ppZY, numpy.transpose(Y[:,1,:]), numpy.transpose(Z[:,1,:]), zdir='x', offset=0, cmap=cm.coolwarm)
        csetY = ax.contour(numpy.transpose(X[:,:,1]), ppXZ, Z[:,1,:] , zdir='y', offset=pBasis.Ly, cmap=cm.coolwarm)
        csetZ = ax.contour(X[:,:,1], Y[:,:,1], ppXY, zdir='z', offset=0, cmap=cm.coolwarm)
    
    
    
    def init():
        animate(0)

    ani = animation.FuncAnimation(fig, animate, numpy.arange(0, len(PhotoCoeff)), init_func=init, interval=500, blit=False,repeat=True)
    
    ax.set_xlabel('X')
    ax.set_xlim(0, pBasis.Lx)
    ax.set_ylabel('Y')
    ax.set_ylim(0, pBasis.Ly)
    ax.set_zlabel('Z')
    ax.set_zlim(0, pBasis.Lz)
    
    
    x,y,z = numpy.meshgrid(numpy.array([0,1]),numpy.array([0,2]),numpy.array([0,1]))
    
    
    Save_Movie=True
    
    if Save_Movie:
#       Set up formatting for the movie files
#       Writer = animation.writers['ffmpeg']
#       writer = Writer(fps=15, metadata=dict(artist='Me'), bitrate=1800)
        FFwriter = animation.FFMpegWriter()
        ani.save("./Photon"+OutputSuffix+".mp4",writer = FFwriter, fps=2, extra_args=['-vcodec', 'h264','-pix_fmt', 'yuv420p'])
#         ani.save('./im.mp4', writer='ffmpeg')
    else:
        plt.show()
