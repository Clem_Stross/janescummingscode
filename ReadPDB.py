#!/usr/bin/python

import numpy
import os
import random

#This function reads a pdb file and outputs a 3D martix of [BChl,Atom,xyz]
#Atom run from Mg N N (of TDM) N N (not in TDM)
#Output is of desired length in BChl and Atom. (see below) 


#pdbFile is an open pdb file to read and pick out BChl atoms
#Ring allows different parts of the LHII to be picked eg "All", "B50", "B800"
#Number of atoms gives part of chorin ring to be selected eg 1 is just Mg, 3 is only TDM atoms N-Mg-N (with TDM as Mg at 0 and N at 1,2), 5 gives chorin ring (with TDM as Mg at 0 and N at 1,2)
def ReadPDB(pdbFile,Ring="B850",NumAtomOutput=3,CentreAndRotate=True):
    
    
    AtomPosFull=numpy.zeros((27,5,3))   #Array for all atomic positions
    
    BChl_Count=0.
    line=pdbFile.readline()
    while line: #For every line in file
        cell=line.split() #split data into cells
        if len(cell)==11 or len(cell)==10 : #only body of PDB
            if cell[3]=="BCL":
                if cell[0]=="ATOM" or cell[0]=="HETATM":  #only for BChl in LHII
                    if cell[2]=="MG":
                        AtomPosFull[int(BChl_Count),0,:]=numpy.array([cell[5],cell[6],cell[7]])
                    if cell[2]=="NB" or cell[2]=="N_B":
                        AtomPosFull[int(BChl_Count),1,:]=numpy.array([cell[5],cell[6],cell[7]])
                    if cell[2]=="ND" or cell[2]=="N_D":
                        AtomPosFull[int(BChl_Count),2,:]=numpy.array([cell[5],cell[6],cell[7]])
                        BChl_Count=BChl_Count+1 #with final N counter moves on
                    if cell[2]=="NA" or cell[2]=="N_A":
                        AtomPosFull[int(BChl_Count),3,:]=numpy.array([cell[5],cell[6],cell[7]])
                    if cell[2]=="NC" or cell[2]=="N_C":
                        AtomPosFull[int(BChl_Count),4,:]=numpy.array([cell[5],cell[6],cell[7]])            
        line=pdbFile.readline()
    
    
    #Sellect BChl contained in desired output
    if Ring in ("All","LHII",27):
        NumBChl=27
        BChlIndexList=[BChlIndex for BChlIndex in range(0,27,1) if BChlIndex not in range(1,27,3)]+(range(1,27,3))  #order BChl
    elif Ring=="B800" or Ring==9:
        BChlIndexList=range(1,27,3)
        NumBChl=9
    elif Ring=="B850" or Ring==18:
        BChlIndexList=[BChlIndex for BChlIndex in range(0,27,1) if BChlIndex not in range(1,27,3)]
        NumBChl=18
    else:
        print "Ring not recognised in 'ReadPDB'"
    
    
    #Tell user in output with NumAtomOutput 'means' anything
    if NumAtomOutput!=1 and NumAtomOutput!=3 and NumAtomOutput!=5:
        print "Bad choice of 'NumAtomOutput' in 'ReadPDB' as does not give Mg, TDM, or Chorin Ring atoms"
    
    
    #Shrink AtomPosFull into AtomPos
    AtomPos=numpy.zeros((NumBChl,NumAtomOutput,3))    
    for i,BChlIndex in enumerate(BChlIndexList):
        AtomPos[i,:,:]=AtomPosFull[BChlIndex,0:NumAtomOutput,:]
    
    return AtomPos



def CentreAndRotate_Fuction(Coord,*CoordCentre):
    
    
    if len(CoordCentre)==0:
        CoordCentre=Coord
    elif len(CoordCentre)==1:
        CoordCentre=CoordCentre[0]
    else:
        print "'CoordCentre' wrong in 'CentreAndRotate_Fuction' in 'ReadPDB'"
        
    
    #Centre system on [0,0,0]
    Coord=Coord-numpy.mean(CoordCentre[:,0,:],axis=0)
    
    #Find ring normal by cross product
    NormVec=numpy.zeros(3)
    for i,BChl_i in enumerate(Coord[:,0,:]):
        for BChl_j in Coord[i+1:,0,:]:
            Vec=numpy.cross(BChl_i,BChl_j)
            #Get sign right
            if Vec[-1]>0:
                NormVec=NormVec+Vec
            elif Vec[-1]<0:
                NormVec=NormVec-Vec
    NormVec=NormVec/numpy.linalg.norm(NormVec)  #normalise
    
    #Find new coordinate systems by GramSchmidt process
    NewZ=NormVec
    NewX=numpy.array([1,0,0])-(NewZ[0]*NewZ)
    NewY=numpy.array([0,1,0])-(NewZ[1]*NewZ)-(NewX[1]*NewX)
    
    #Set to new coordinate systems
    for i,BChl in enumerate(Coord):
        for j,Atom in enumerate(BChl):
            Coord[i,j,0]=numpy.dot(NewX,Atom)
            Coord[i,j,1]=numpy.dot(NewY,Atom)
            Coord[i,j,2]=numpy.dot(NewZ,Atom)
    

    return Coord
    


#Find all files in the folder DirectoryPath, which contain the text in Extension
def FindFiles(DirectoryPath,Extension):
    
    FileList=[]
    for Files in os.listdir(DirectoryPath):
        if Extension in Files:
            FileList.append(Files)
            
    return FileList






def main():
    
    ##Choosing directory containg pdb's
    #DirectoryPath="../../"
    DirectoryPath='/Volumes/curie/chmwvdk/projects/lhc2/md/1/'
    
    FileList=FindFiles(DirectoryPath,".pdb")
    
    File=random.choice(FileList)    #pick random file from FileList
    AtomPos=ReadPDB(open(DirectoryPath+File,"r"),"B850",3)    #Save coordinates from pbd
    AtomPos=CentreAndRotate_Fuction(AtomPos)
    
    
    numpy.savetxt("../Outputs/Single/Coord.txt",numpy.reshape(AtomPos, (AtomPos.shape[0]*AtomPos.shape[1],AtomPos.shape[2])), "%.4g")

if __name__=="__main__": main()