'''
Created on Dec 4, 2015

@author: cs9114
'''

import sys
import numpy

import math

from JaynesCummings_Basis import JaynesCummings_Basis


from Read_InputFile import ReadFromFile

from LHII_Ham import LHII_Ham

from Propagate import PropagateState_WF_Fast


from Mixing import Mixing_WF
from Mixing import Plot_Mixing_Multi


from Localisation import Localisation_InvParLength
from Localisation import Plot_Localisation_InvParLength_Multi

from progressbar import *

import matplotlib.pyplot as plt


        
def Pulse_Multi_main(InFile,NumOfRuns):
        
    OutputSuffix=ReadFromFile("OutputSuffix",InFile,"String","")
    if len(OutputSuffix)>0:
        OutputSuffix="_"+OutputSuffix
    
    L=ReadFromFile("L",InFile,"List")
    
    
    LocBasisShrink=ReadFromFile("LocBasisShrink",InFile,"Bool",False)
    EPol=ReadFromFile("EPol",InFile,"Bool",True)
    k_pm=ReadFromFile("k_pm",InFile,"Bool",True)
    PulsePol=ReadFromFile("PulsePol",InFile,"List",0)
    if type(PulsePol)!=numpy.ndarray:
            PulsePol=numpy.array([0,1,0])
    else:
        PulsePol=PulsePol/numpy.linalg.norm(PulsePol)
    
    
    Omega=ReadFromFile("Omega",InFile,"Float")
    PulseProbWidth=ReadFromFile("PulseProbWidth",InFile,"Float")
    dOmega=ReadFromFile("deltaOmega",InFile,"Float")
    
    
    
    tMax=ReadFromFile("tMax",InFile,"Float",0)
    tStep=ReadFromFile("tStep",InFile,"Float",0.05)
    if tMax==0:
        tMax=(PulseProbWidth*8)/137
    
    pState=JaynesCummings_Basis(L[0],L[1],L[2])
    pState.MakeBasis(Omega, dOmega,EPol,k_pm) #Give \omega and \Delta \omega -> Produce allowed states
    print("nPhot=", pState.nPhot,"nPhotE=", pState.nPhotE,"nStep=",int(tMax/tStep))
    
    if pState.Dim=="1D":
        kCoff=pState.Pulse_1D_kSpace(Omega/137,PulseProbWidth,(L[0]/2)-(3*PulseProbWidth),PulsePol)
    elif pState.Dim=="2D":
        kCoff=pState.Pulse_2D_kSpace(Omega/137,numpy.array([1,1,0]),PulseProbWidth,PulseProbWidth,pState.L/2-(numpy.array([1,1,0])*3*PulseProbWidth),numpy.array([-1,1,0]))

    
    
    widgets = ['Pulse Multi Run: ', Percentage(), ' ', Bar(marker='0',left='[',right=']'),' ', ETA(), ' ', FileTransferSpeed()] #see docs for other options
    pbar = ProgressBar(widgets=widgets, maxval=NumOfRuns)
    pbar.start()
    
    
    
    Mixing_All=numpy.zeros(shape=(NumOfRuns,int(tMax/tStep)))
    Mixing_e_All=numpy.zeros(shape=(NumOfRuns,int(tMax/tStep)))
    InvParLength_All=numpy.zeros(shape=(NumOfRuns,int(tMax/tStep)))
    for Run in range(0,NumOfRuns):
        
        Ham,Mu,Rsite=LHII_Ham(InFile)
        Rsite=Rsite+L/2
        
        pState.ReadInSysInfo(Ham,Mu,Rsite,LocBasisShrink)
        pState.BuildHam_JanesCummings_1p(False) #build hamiltonian
        IState=numpy.zeros(shape=(pState.nState),dtype=complex) #InitialState, single excited site
        IState[0:pState.nPhotE]=kCoff
        
        State=PropagateState_WF_Fast(pState.Ham_JC,IState,tMax,tStep,1) #Propergate through time
    
        #Might want to add logic here
        SitePop=numpy.absolute(State[:,pState.nPhotE:pState.nState])**2
        EigenPop=numpy.zeros(shape=(SitePop.shape))
    
        for t in range(0,EigenPop.shape[0],1):
            EigenPop[t]=numpy.absolute(numpy.dot(pState.U,State[t,pState.nPhotE:pState.nState]))**2
        
        Mixing,Mixing_e=Mixing_WF(State[:,pState.nPhotE:pState.nState])
        InvParLength=Localisation_InvParLength(SitePop)
        
        Mixing_All[Run,:]=Mixing
        Mixing_e_All[Run,:]=Mixing_e
        InvParLength_All[Run,:]=InvParLength
        
        pbar.update(Run)
    pbar.finish()
    
    
    MixingFig_Multi=Plot_Mixing_Multi(Mixing_All,Mixing_e_All,tMax,tStep)
    MixingFig_Multi.savefig('./MixingFig_Multi'+OutputSuffix+'.pdf')
    
    InvParLengthFig_Multi=Plot_Localisation_InvParLength_Multi(InvParLength_All,tMax,tStep)
    InvParLengthFig_Multi.savefig('./InvParLength_Multi'+OutputSuffix+'.pdf')
    
    print "Job Done :-)"
        
        
        
        
if __name__ == '__main__':
    if len(sys.argv)==1:
        print("no input file")
        sys.exit()
    elif len(sys.argv)==2:
        print("File and NumOfRuns needed")
        sys.exit()
        
    elif len(sys.argv)==3:
        NumOfRuns=int(sys.argv[2])
        if NumOfRuns==0:
            print("NumOfRuns=0")
            sys.exit()
        else:
            Pulse_Multi_main(sys.argv[1],NumOfRuns)
    else:
        print "Too many inputs"
        sys.exit()