#!/usr/bin/python

import numpy
import sys
import math
import random

class LHII_Structure():
    
    def __init__(self):
        self.BChl_Num_B800=9
        self.BChl_Num_B850=18
    
    #reads in atomic coord from 3D matrix of atomic positions
    #saves self.MgPos
    #saves self.MuNorm
    #saves self.Coord
    def SetAtomicCoord(self,Coord):
        
        self.BChl_Num=len(Coord)
        self.ChlorPerLHII=len(Coord)
        
        if self.BChl_Num<=self.BChl_Num_B800+self.BChl_Num_B850:
            self.LHII_Num=1
        
        if Coord.shape[1]>=1:   #save Mg positions used to find R_{ij}
            self.MgPos=Coord[:,0,:]
        else:
            print "Mg Pos not found in 'SetAtomicCoord' in 'FormHam'"
            
            
        if Coord.shape[1]>=3:   #save vector for transition dipole moment
            self.MuNorm=Coord[:,1,:]-Coord[:,2,:]
            for BChlIndex,TDM in enumerate(self.MuNorm):
                self.MuNorm[BChlIndex]=TDM/numpy.linalg.norm(TDM)   #normalis transtion dipol moment
        else:
            print "Mg transition dipole moment not found in 'SetAtomicCoord' in 'FormHam'"
        
        self.Coord=Coord    #saving all coord
        
        
        
        
    def SetAtomicCoord_IdealLHII(self, BChl_Num=18):
        
        if BChl_Num in (9,18,27):
            self.BChl_Num=BChl_Num
            self.ChlorPerLHII=BChl_Num
            if self.BChl_Num<=self.BChl_Num_B800+self.BChl_Num_B850:
                self.LHII_Num=1
        else:
            self.BChl_Num=BChl_Num
            self.ChlorPerLHII=BChl_Num
            print "'BChl_Num' incorrect in 'SetAtomicCoord_IdealLHII' in 'FormHam.py'"
#             sys.exit()
        

            
        
        
#         phi=random.uniform(0,math.pi*(2/9))
        phi=0
        
        if self.BChl_Num in (18,27):
            B850_R=26
            B850_MgPos=numpy.zeros(shape=(18,3))
            B850_TDM=numpy.zeros(shape=(18,3))
            B850_Theta=2*math.pi/18
            for BChl in range(0,18,1):
                B850_MgPos[BChl]=B850_R*numpy.array([math.cos((BChl*B850_Theta)+phi),math.sin((BChl*B850_Theta)+phi),0])
                B850_TDM[BChl]=numpy.array([-math.sin((BChl*B850_Theta)+phi),math.cos((BChl*B850_Theta)+phi),0])*(-1)**BChl
        
        if self.BChl_Num in (9,27):
            B800_R=33
            B800_MgPos=numpy.zeros(shape=(9,3))
            B800_TDM=numpy.zeros(shape=(9,3))
            B800_Theta=2*math.pi/9
            for BChl in range(0,9,1):
                BChl=BChl+0.5
                B800_MgPos[BChl]=B800_R*numpy.array([math.cos((BChl*B800_Theta)+phi),math.sin((BChl*B800_Theta)+phi),0])
                B800_TDM[BChl]=numpy.array([-math.sin((BChl*B800_Theta)+phi),math.cos((BChl*B800_Theta)+phi),0])
            
            
        if self.BChl_Num==18:
            self.MgPos=B850_MgPos
            self.MuNorm=B850_TDM
            
        if self.BChl_Num==9:
            self.MgPos=B800_MgPos
            self.MuNorm=B800_TDM
                
        if self.BChl_Num==27:
            self.MgPos=numpy.vstack((B850_MgPos,B800_MgPos))
            self.MuNorm=numpy.vstack((B850_TDM,B800_TDM))
            
            
        
        if self.BChl_Num not in (9,18,27):
            print "'BChl_Num' incorrect in 'SetAtomicCoord_IdealLHII' in 'FormHam.py'"
            self.BChl_Num=BChl_Num
            self.ChlorPerLHII=BChl_Num
            R=26
            MgPos=numpy.zeros(shape=(self.BChl_Num,3))
            TDM=numpy.zeros(shape=(self.BChl_Num,3))
            Theta=2*math.pi/self.BChl_Num
            for BChl in range(0,self.BChl_Num,1):
                MgPos[BChl]=R*numpy.array([math.cos((BChl*Theta)+phi),math.sin((BChl*Theta)+phi),0])
                TDM[BChl]=numpy.array([-math.sin((BChl*Theta)+phi),math.cos((BChl*Theta)+phi),0])*(-1)**BChl
                self.MgPos=MgPos
                self.MuNorm=TDM
        
        
        
        
        
        
    
    #Saves all Ei and Mui from a normal distribution of values
    #saves self.E
    #saves self.MuMag
    def SetHamPram(self,E0,Delta_E0,Mu0,Delta_Mu0):
        
        if type(E0) in (int,float):
            E0=[E0,E0]
        if type(Delta_E0) in (int,float):
            Delta_E0=[Delta_E0,Delta_E0]
            
        self.E=numpy.zeros(0)
        
        Delta_E0_AU=0.001322 #Delta_E0=0.001322
        Delta_Mu0_AU=0.0641    #Delta_Mu0=0.0641
        
        if self.BChl_Num==self.BChl_Num_B850 or self.BChl_Num==self.BChl_Num_B850+self.BChl_Num_B800:
            if Delta_E0[0]==0: #is StDev=0, constant array
                self.E=numpy.hstack((self.E,E0[0]*numpy.ones(self.BChl_Num_B850)))
            elif Delta_E0[0]!=0: #is StDev!=0, normal distribution
                self.E=numpy.hstack((self.E,numpy.random.normal(E0[0],Delta_E0[0]*Delta_E0_AU,self.BChl_Num_B850)))
                
        if self.BChl_Num==self.BChl_Num_B800 or self.BChl_Num==self.BChl_Num_B850+self.BChl_Num_B800:
            if Delta_E0[1]==0: #is StDev=0, constant array
                self.E=numpy.hstack((self.E,E0[1]*numpy.ones(self.BChl_Num_B800)))
            elif Delta_E0[1]!=0: #is StDev!=0, normal distribution
                self.E=numpy.hstack((self.E,numpy.random.normal(E0[1],Delta_E0[1]*Delta_E0_AU,self.BChl_Num_B800)))
        
        if self.BChl_Num!=self.BChl_Num_B850 and self.BChl_Num!=self.BChl_Num_B800 and self.BChl_Num!=self.BChl_Num_B850+self.BChl_Num_B800:
            print "'BChl_Num' incorrect in 'SetHamPram' in 'FormHam.py'"
            if Delta_E0[0]==0: #is StDev=0, constant array
                self.E=E0[0]*numpy.ones(self.BChl_Num)
            else: #is StDev!=0, normal distribution
                self.E=numpy.random.normal(E0[0],Delta_E0[0]*Delta_E0_AU,self.BChl_Num)
                
                
                
        if Delta_Mu0==0: #is StDev=0, constant array
            self.MuMag=Mu0*numpy.ones(self.BChl_Num)
        else: #is StDev!=0, normal distribution
            self.MuMag=numpy.random.normal(Mu0,Delta_Mu0*Delta_Mu0_AU,self.BChl_Num)



    #takes likes of FormHam objects and returns new FormHam which is a combination on inputs
    def Combine_LHII_Structures(self,*LHIIs):
        
        if not hasattr(self, 'BChl_Num'):
            self.BChl_Num=0
            self.MgPos=numpy.zeros(shape=(0,3))
            self.E=numpy.zeros(shape=(0))
            self.MuNorm=numpy.zeros(shape=(0,3))
            self.MuMag=numpy.zeros(shape=(0))
        
        for Struc in LHIIs:
            self.BChl_Num+=Struc.BChl_Num
            self.E=numpy.hstack((self.E,Struc.E))
            self.MuMag=numpy.hstack((self.MuMag,Struc.MuMag))
            self.MgPos=numpy.vstack((self.MgPos,Struc.MgPos))
            self.MuNorm=numpy.vstack((self.MuNorm,Struc.MuNorm))
                
                
    def Translate_LHII_Structure(self,TransVec):
        for BChl in range(0,self.BChl_Num,1):
            self.MgPos[BChl]=self.MgPos[BChl]+TransVec









    def FormHam(self):
        #E is BChl mean transition energy, with delta_E as the StDev
        #MuMag is BChl mean magnitude of dipole transition moment, with delta_MuMag as the StDev
        
        
        #Form hamiltonian
        self.Ham=numpy.zeros((self.BChl_Num,self.BChl_Num))
        
        #For all BChl
        for BChl_i in range(0,self.BChl_Num,1):
            
            self.Ham[BChl_i,BChl_i]=self.E[BChl_i] #Set site energies
            MuMag_i=self.MuMag[BChl_i]
            MuNorm_i=self.MuNorm[BChl_i]
            
            for BChl_j in range(BChl_i+1,self.BChl_Num,1):
                
                MuMag_j=self.MuMag[BChl_j]
                MuNorm_j=self.MuNorm[BChl_j]
                
                R=self.MgPos[BChl_i]-self.MgPos[BChl_j]   #Find vec from one BChl to the other
                RNorm=R/numpy.linalg.norm(R)    #Find norm of the direction
                RMag=numpy.linalg.norm(R)/0.529 #Convert to bohr
                
                #First non charge terms of the multipole expansion
                Term1=numpy.dot(MuNorm_i,MuNorm_j)/(RMag**3)
                Term2=-3*((numpy.dot(MuNorm_i,RNorm)*numpy.dot(MuNorm_j,RNorm))/(RMag**3))
                
                #Coupling always negitive
                #self.Ham[BChl_i,BChl_j]=-abs((MuMag_i*MuMag_j)*(Term1+Term2))
                self.Ham[BChl_i,BChl_j]=(MuMag_i*MuMag_j)*(Term1+Term2)
                self.Ham[BChl_j,BChl_i]=self.Ham[BChl_i,BChl_j]
        
        
    def HamScaling(self,NeighbourCoupling=300.,Units="Wavenumber"):
        #NeighbourCoupling is the targeted coupling constant for nearest neighbours
        #Units is the units of NeighbourCoupling
        
        #Save site eneries so they do not get scalled
        SiteE=numpy.zeros(self.BChl_Num)
        for BChl in range(0,self.BChl_Num,1):
            SiteE[BChl]=self.Ham[BChl,BChl]
        
        
        Couplings=numpy.zeros(self.BChl_Num_B850)
        for LHII in range(0,self.LHII_Num,1): #Find all nearest neighbours
            for Pairs in range(0,self.BChl_Num_B850,1):
                Index=LHII*self.BChl_Num_B850+Pairs
                Couplings[Index]=self.Ham[Index,(LHII*self.ChlorPerLHII)+((Pairs+1)%self.BChl_Num_B850)]
            
                #make ScalingFactor for correct units, so that the mean is at the inputted vaule
        if Units=="au" or Units=="a.u." or Units=="a.u" or Units=="Atomic Units" or Units=="atomic units": 
            ScalingFactor=abs(NeighbourCoupling/numpy.mean(Couplings))
        elif Units=="Wavenumber" or Units=="wavenumber" or Units=="Wavenumbers" or Units=="wavenumbers" or  Units=="cm^-1" or Units=="cm-1":
            ScalingFactor=abs(NeighbourCoupling/(2.194763*(10**5))/numpy.mean(Couplings))
        elif Units=="Joules" or Units=="joules" or  Units=="J":
            ScalingFactor=abs(NeighbourCoupling/(4.359782*(10**-18))/numpy.mean(Couplings))
        else:
            ScalingFactor=1
            print "Units not recognised, Use 'Atomic Units' or 'Wavenumber'"
        
        self.Ham=self.Ham*ScalingFactor
        
        #Rewriting site eneries so they do not get scalled
        for BChl in range(0,self.BChl_Num,1):
            self.Ham[BChl,BChl]=SiteE[BChl] 
    
    
    def SaveNearestNeighbourCoupling(self,Units="Wavenumber"):
        
        self.NN_Couplings=numpy.zeros(self.BChl_Num_B850)
        for LHII in range(0,self.LHII_Num,1): #Find all nearest neighbours
            for Pairs in range(0,self.BChl_Num_B850,1):
                Index=LHII*self.BChl_Num_B850+Pairs
                self.NN_Couplings[Index]=self.Ham[Index,(LHII*self.ChlorPerLHII)+((Pairs+1)%self.BChl_Num_B850)]
        
        self.NN_Couplings=self.NN_Couplings*(2.194763*(10**5))
        
        
    
    
    def RotateLHII(self,Norm):
        Norm=Norm/numpy.linalg.norm(Norm)
        
        cNorm=numpy.zeros(shape=(3)) #current norm
        
        if self.BChl_Num==27:
            ItNum=18
        else:
            ItNum=self.BChl_Num
        
        for BChl_1 in range(0,ItNum,1):
            for BChl_2 in range(BChl_1+1,ItNum,1):
                vHold=numpy.cross(self.MgPos[BChl_1],self.MgPos[BChl_2])
                if numpy.dot(vHold,cNorm)>=0:
                    cNorm+=vHold
                else:
                    cNorm-=vHold
        cNorm=cNorm/numpy.linalg.norm(cNorm)
        
        
        #from http://math.stackexchange.com/questions/180418/calculate-rotation-matrix-to-align-vector-a-to-vector-b-in-3d
        v=numpy.cross(cNorm,Norm)
        s=numpy.linalg.norm(v)
        c=numpy.dot(cNorm,Norm)
        
        vx=numpy.array([[0,-v[2],v[1]],[v[2],0,-v[0]],[-v[1],v[0],0]])
        RotMatrix1=numpy.eye(3)+vx+(numpy.dot(vx,vx)*((1-c)/s**2)) #rotates so ring norm in at Norm
        
        
        
        
        
        #Rotating the ring around this norm to remove orentational biasing.
        #rotating around random vector from http://math.stackexchange.com/questions/511370/how-to-rotate-one-vector-about-another
        y=numpy.array([0,1,0])
        z=numpy.array([0,0,1])
        
        GramSch1=Norm
        GramSch2=y-(numpy.dot(GramSch1,y)*GramSch1)
        GramSch2=GramSch2/numpy.linalg.norm(GramSch2)
        GramSch3=z-(numpy.dot(GramSch1,z)*GramSch1)-(numpy.dot(GramSch2,z)*GramSch2)
        GramSch3=GramSch3/numpy.linalg.norm(GramSch3)
        
        Phi=random.uniform(0,2*math.pi)
        U=numpy.array([GramSch1,GramSch2,GramSch3])
        A_Matrix=numpy.array([[1,0,0],[0,math.cos(Phi),-math.sin(Phi)],[0,math.sin(Phi),math.cos(Phi)]])
        RotMatrix2=numpy.dot(numpy.linalg.inv(U),numpy.dot(A_Matrix,U))
        
        
        #First rotate to line up norm, then spin around norm
        RotMatrix=numpy.dot(RotMatrix2,RotMatrix1)
        
        for BChl in range(0,self.BChl_Num,1):
            self.MgPos[BChl]=numpy.dot(RotMatrix,self.MgPos[BChl])
            self.MuNorm[BChl]=numpy.dot(RotMatrix,self.MuNorm[BChl])
        
def main():
    pass

 
if __name__=="__main__": main()