'''
Created on Oct 19, 2015

@author: cs9114
'''
c=137
import numpy
import math
import sys

class JaynesCummings_Basis(object):
    '''
    classdocs
    '''


    def __init__(self,Lx,Ly,Lz ):
        '''
        Constructor
        '''
        #Form Box
        self.Lx=float(Lx)
        self.Ly=float(Ly)
        self.Lz=float(Lz)
        self.L=numpy.array([self.Lx,self.Ly, self.Lz])
        self.Vol=self.Lx*self.Ly*self.Lz
        
        
    
    def ReadInBasisInfo(self,Omega,dOmega=0.0,ForceBoxDim="",dEMag=0.0 ,MuMag=0.0):
        #=======================================================================
        # Find allowed n values, and save there energies and k vectors
        #=======================================================================
        
        assert ForceBoxDim in [""," ","1D","1d","1D","1d"]
        assert dEMag>=0
        assert MuMag>=0
        
        if dOmega==0.0:
            DelOmega=Omega
        elif dOmega>0.0:
            DelOmega=dOmega+dEMag
            
        
            
        
        
        #As to truncate the photo basis states
#         nMax=self.L*((Omega+DelOmega)/(c*math.pi))
        self.nMax=self.L*((Omega+DelOmega)/(c*math.pi*2)) #\cite{Loudon} page 130
        self.OmegaMax=Omega+DelOmega
        self.OmegaMin=Omega-DelOmega
        
        if max(self.L*((2*DelOmega)/(c*math.pi*2)))<1:
                print "Box too small or DelOmega too small"
                sys.exit()
        
        if ForceBoxDim in [""," "]:
            if self.nMax[0]<=1 and self.nMax[1]<=1 and self.nMax[2]<=1:
                print "Box too small"
                sys.exit()
            elif self.nMax[0]>1 and self.nMax[1]<=1 and self.nMax[2]<=1:
                self.Dim="1D"
            elif self.nMax[0]>1 and self.nMax[1]>1 and self.nMax[2]<=1:
                self.Dim="2D"
            elif self.nMax[0]>1 and self.nMax[1]>1 and self.nMax[2]>1:
                self.Dim="3D"
            else:
                print "Increase box lengths in order of x,y then z"
                sys.exit()
        elif ForceBoxDim in ["1D","1d"]:
            self.nMax[1]=0.1 #Any number >0 but <1
            self.nMax[2]=0.1
            self.Dim="1D"
        elif ForceBoxDim in ["2D","2d"]:
            self.nMax[2]=0.1
            self.Dim="2D"
        elif ForceBoxDim in ["3D","3d"]:
            self.Dim="3D"
        
        
        
        #Auto generation of DelOmega
        #http://read.pudn.com/downloads158/sourcecode/others/707619/Spontaneous.pdf
        if dOmega==0.0:
            assert MuMag>0
            if self.Dim=="1D":
                EinsteinA=Omega*(MuMag**2)*4*math.pi/(self.Ly*self.Lz*137)
            elif self.Dim=="2D":
                EinsteinA=(Omega**2)*(MuMag**2)*2*math.pi/(self.Lz*(137**2))
            elif self.Dim=="3D":
                EinsteinA=(Omega**3)*(MuMag**2)*4/(3*(137**3))
            
            
            if EinsteinA<(c*math.pi*2)/max(self.L):
                print "warning dOmega generated is too small. Extend box size."
                print "c*math.pi*2", EinsteinA, "vs  Egap", (c*math.pi*2)/max(self.L)
            
            DelOmega=(10*EinsteinA)
            self.ReadInBasisInfo(Omega,DelOmega,ForceBoxDim,dEMag,MuMag)
        
            
        
        
        
    def MakeBasis(self,EPol=True,k_pm=True):
        
        self.kVec=numpy.zeros(shape=(0,3)) #k vectors within the box
        self.kMag=numpy.zeros(shape=(0))
        
        
        #Finding allowed energy levels (could probably be speeded up)
        for nx in range(0,int(math.ceil(self.nMax[0])),1):
            for ny in range(0,int(math.ceil(self.nMax[1])),1):
                for nz in range(0,int(math.ceil(self.nMax[2])),1):
#                     kVec=math.pi*numpy.array([nx,ny,nz])/self.L
                    kVec=2*math.pi*numpy.array([nx,ny,nz])/self.L
                    kMag=numpy.linalg.norm(kVec)
                     
                     
                    if c*kMag<(self.OmegaMax) and c*kMag>(self.OmegaMin) and c*kMag>0:
 
                        self.kVec=numpy.append(self.kVec,numpy.array([kVec]),axis=0)
                        self.kMag=numpy.append(self.kMag,kMag)
                        
                        
                        
                         
                        if nx*ny*nz>0: #All greater than 0
                            self.kVec=numpy.append(self.kVec,numpy.array([[-kVec[0],kVec[1],kVec[2]]]),axis=0)
                            self.kMag=numpy.append(self.kMag,kMag)
                        if ny*nx>0: #x and y greater than 0
                            self.kVec=numpy.append(self.kVec,numpy.array([[kVec[0],-kVec[1],kVec[2]]]),axis=0)
                            self.kMag=numpy.append(self.kMag,kMag)
                        if nz*(nx+ny)>0: #z and not either x or y greater than 0
                            self.kVec=numpy.append(self.kVec,numpy.array([[kVec[0],kVec[1],-kVec[2]]]),axis=0)
                            self.kMag=numpy.append(self.kMag,kMag)
                            
        if k_pm==True:
            self.kVec=numpy.ravel(numpy.column_stack((self.kVec,-self.kVec))).reshape(len(self.kVec)*2,3)
            self.kMag=numpy.ravel(numpy.column_stack((self.kMag,self.kMag)))
            self.k_pm=True
        else:
            self.k_pm=False
            
        self.nPhot=len(self.kVec)
        self.Omega=self.kMag*c
        
        if EPol==True:
            self.EPol=True
            self.Set_ElecVec() #Set two possible electric vectors for photon states
            self.nPhotE=2*self.nPhot
        elif EPol==False:
            self.EPol=False
            self.nPhotE=self.nPhot
        
        
    def Set_ElecVec(self):
        #=======================================================================
        # Find 2 perpendicular allowed electric vector for each state.
        #=======================================================================
        self.ElecVec=numpy.zeros(shape=(self.nPhot,2,3))
        
        for Phot in range(0,self.nPhot,1):
            eVec1=numpy.cross(numpy.array([0.0,0.0,1.0]),self.kVec[Phot])
            eVec1Norm=numpy.linalg.norm(eVec1)
            if eVec1Norm>10**-6: #This is true assuming kVec != \pm(0,0,1)
                self.ElecVec[Phot,0,:]=eVec1/eVec1Norm
                eVec2=numpy.cross(self.ElecVec[Phot,0],self.kVec[Phot])
                self.ElecVec[Phot,1,:]=eVec2/numpy.linalg.norm(eVec2)
            elif eVec1Norm<10**-6: #This is true if kVec == \pm(0,0,1)
                self.ElecVec[Phot,0,:]=numpy.array([0.0,1.0,0.0])
                self.ElecVec[Phot,1,:]=numpy.array([1.0,0.0,0.0])
            
        
    
    def ReadInSysInfo(self,HamSys,mu_Site,rSite=0,LocBasisShrink=False):
        #=======================================================================
        # This reads in the information about the system.
        # rSite is the location of the sites in the system. If rSite=0 then all sites are put in the center of the box
        # HamSys can be diagnol or not, mu_Site are the transition dipole moments in the Ham basis
        #=======================================================================
        
        nSys=len(HamSys)
        if type(rSite)==int:
            rSite=numpy.zeros(shape=(0,3))
            for Site in range(0,nSys,1):
                rSite=numpy.append(rSite,[self.L/2],axis=0)
        
        
        if (nSys,nSys)!=HamSys.shape or (nSys,3)!=mu_Site.shape or (nSys,3)!=rSite.shape:
            print "inconsistencies in sizes in 'ReadInSysInfo'"
            print "HamSys.shape=", HamSys.shape
            print "mu_Site.shape=", mu_Site.shape
            print "rSite.shape=", rSite.shape
            sys.exit()
        
        self.nSys=nSys
        self.Ham_Site=HamSys
        self.mu_Site=mu_Site
        self.E_Site=self.Ham_Site.diagonal()
        self.rSite=rSite
        self.LocBasisShrink=LocBasisShrink
        
        #=======================================================================
        # Holds the information to diagonalisation HamSys, this means the resulting data can be for eigenstates or sites
        #=======================================================================
        Holder=numpy.linalg.eigh(self.Ham_Site)
        self.E_k=Holder[0]
        self.U=numpy.transpose(Holder[1])

        
        self.mu_k=numpy.zeros(shape=(self.nSys,3))
        for k in range(0,self.nSys,1):
            for j in range(0,self.nSys,1):
                self.mu_k[k]+=(self.mu_Site[j]*self.U[k,j])
        
    
    
    

    
    def BuildHam_JanesCummings_1p(self,UseEingenstate=False):
        
        #=======================================================================
        #If UseEingenstate==False use imput Ham
        #If UseEingenstate==True use diagonal Ham
        #=======================================================================
        
        self.nState=self.nPhotE+self.nSys
        
        
#         self.Ham_JC=numpy.zeros(shape=(self.nState,self.nState))
        self.Ham_JC=numpy.zeros(shape=(self.nState,self.nState),dtype=complex)
        
        if UseEingenstate==False:
            self.HamSys=self.Ham_Site
            self.muSys=self.mu_Site
            self.E_Sys=self.HamSys.diagonal()
        elif UseEingenstate==True:
            self.HamSys=numpy.diag(self.E_k)
            self.muSys=self.mu_k
            self.E_Sys=self.HamSys.diagonal()
        else:
            print "Error in 'UseEingenstate' in 'BuildHam_JanesCummings_1p'"
            sys.exit()
        self.Ham_JC[self.nPhotE:self.nState,self.nPhotE:self.nState]=self.HamSys
        
        
        
                        
                        
        if self.EPol==True:
            for Phot in range(0,self.nPhot,1):
#                 PerFac=(self.Omega[Phot]*4*math.pi/self.Vol)**0.5
                PerFac=(self.Omega[Phot]*2*math.pi/self.Vol)**0.5
                for Pol in (0,1):
                    self.Ham_JC[(2*Phot)+Pol,(2*Phot)+Pol]=self.Omega[Phot]
                    for Site in range(0,self.nSys,1):
                        GeomFac=numpy.dot(self.ElecVec[Phot,Pol],self.muSys[Site])
                        SpaceFac=numpy.exp(1j*numpy.dot(self.kVec[Phot],self.rSite[Site]))
#                         SpaceFac=numpy.sin(numpy.dot(self.kVec[Phot],self.rSite[Site]))
#                         SpaceFac=1


#                         self.Ham_JC[(2*Phot)+Pol,self.nState-self.nSys+Site]=-PerFac*SpaceFac*GeomFac
#                         self.Ham_JC[self.nState-self.nSys+Site,(2*Phot)+Pol]=numpy.conjugate(self.Ham_JC[(2*Phot)+Pol,self.nState-self.nSys+Site])
                        
                        
                        
                        self.Ham_JC[(2*Phot)+Pol,self.nState-self.nSys+Site]=1j*PerFac*SpaceFac*GeomFac
                        self.Ham_JC[self.nState-self.nSys+Site,(2*Phot)+Pol]=numpy.conjugate(self.Ham_JC[(2*Phot)+Pol,self.nState-self.nSys+Site])
        
        if self.EPol==False:
            for Phot in range(0,self.nPhot,1):
                PerFac=((self.Omega[Phot]*4*math.pi)/self.Vol)**0.5
                PerFac=((self.Omega[Phot]*2*math.pi)/self.Vol)**0.5
                self.Ham_JC[Phot,Phot]=self.Omega[Phot]
                for Site in range(0,self.nSys,1):
                        GeomFac=numpy.absolute(1-numpy.dot(self.kVec[Phot]/numpy.linalg.norm(self.kVec[Phot]),self.muSys[Site]/numpy.linalg.norm(self.muSys[Site]))**2)**0.5
                        SpaceFac=numpy.exp(1j*numpy.dot(self.kVec[Phot],self.rSite[Site]))
#                         SpaceFac=numpy.cos(numpy.dot(self.kVec[Phot],self.rSite[Site]))
                        self.Ham_JC[Phot,self.nState-self.nSys+Site]=-PerFac*SpaceFac*GeomFac*numpy.linalg.norm(self.muSys[Site])
                        self.Ham_JC[self.nState-self.nSys+Site,Phot]=numpy.conjugate(self.Ham_JC[Phot,self.nState-self.nSys+Site])
                        
        
        
            
    def Pulse_1D_kSpace(self,k_Pulse,Width_x,x0,Pol=0):
        
        ii=1j
        
#         kCoff=numpy.exp(-((self.kVec[:,0]-k_Pulse)**2)*(Width_x**2))*numpy.exp(ii*x0*(k_Pulse-self.kVec[:,0]))
#         kCoff+=numpy.exp(-((self.kVec[:,0]+k_Pulse)**2)*(Width_x**2))*numpy.exp(-ii*x0*(k_Pulse+self.kVec[:,0]))

        kCoff=numpy.exp(-((self.kVec[:,0]+k_Pulse)**2)*(Width_x**2))*numpy.exp(ii*x0*(k_Pulse+self.kVec[:,0]))
        kCoff=kCoff/numpy.linalg.norm(kCoff)
            
        if self.EPol==False:
            pass
         
        elif self.EPol==True and len(Pol)==3:
            if Pol[0]>0.0001:
                print "Pol and k_PulseDir not perpendicular in Pulse_2D_kSpace"
                Pol[0]=0
            Pol=Pol/numpy.linalg.norm(Pol)
            kCoff=numpy.ravel(numpy.column_stack((kCoff,kCoff)))
            for mode in range(0,self.nPhotE,1):
                    kCoff[mode]=kCoff[mode]*numpy.dot(self.ElecVec[int(mode/2),int(mode%2)],Pol)
                     
        elif self.EPol==True:
            print "No polarisation in Pulse_2D_kSpace"
            kCoff=numpy.ravel(numpy.column_stack((kCoff,kCoff)))
            
        return kCoff
            
    

    def Pulse_2D_kSpace(self,k_PulseMag,k_PulseDir,Width_para,Width_perp,r0,Pol=0):
        
        k_PulseDir=k_PulseDir/numpy.linalg.norm(k_PulseDir)
        if k_PulseDir[0]>0:
            theta=math.atan(k_PulseDir[1]/k_PulseDir[0])
        elif k_PulseDir[0]<0:
            theta=math.atan(k_PulseDir[1]/k_PulseDir[0])+math.pi
        elif k_PulseDir[0]==0 and k_PulseDir[1]>0:
            theta=math.pi/2
        elif k_PulseDir[0]==0 and k_PulseDir[1]<0:
            theta=math.pi*(3/2)
        
        
#         U_kvec=numpy.array([[math.cos(theta),math.sin(theta),0],[-math.sin(theta),math.cos(theta),0],[0,0,1]])
        U_kvec=numpy.array([[math.cos(theta),-math.sin(theta),0],[math.sin(theta),math.cos(theta),0],[0,0,1]])
        
        R0=numpy.dot(r0,U_kvec)
        
#         k_PulseVec=(k_PulseDir*k_PulseMag)/numpy.linalg.norm(k_PulseDir)
        
        
        kCoff=numpy.zeros(shape=(self.nPhot),dtype=complex)
        
        for Mode in range(0,self.nPhot,1):
            
            holder_para=numpy.exp(-((numpy.dot(self.kVec[Mode],U_kvec)[0]-k_PulseMag)**2)*(Width_para**2))*numpy.exp(-1j*R0[0]*(numpy.dot(self.kVec[Mode],U_kvec)[0]+k_PulseMag))
            holder_perp=numpy.exp(-((numpy.dot(self.kVec[Mode],U_kvec)[1]-0)**2)*(Width_perp**2))*numpy.exp(-1j*R0[1]*(numpy.dot(self.kVec[Mode],U_kvec)[1]+0))
            
            
            kCoff[Mode]=holder_para*holder_perp
        
        
        
        if self.EPol==False:
            pass
         
        elif self.EPol==True and type(Pol)!=int:
            if numpy.dot(Pol,k_PulseDir)>0.0001:
                print "Pol and k_PulseDir not perpendicular in Pulse_2D_kSpace"
            Pol=Pol/numpy.linalg.norm(Pol)
            kCoff=numpy.ravel(numpy.column_stack((kCoff,kCoff)))
            for mode in range(0,self.nPhotE,1):
                    kCoff[mode]=kCoff[mode]*numpy.dot(self.ElecVec[int(mode/2),int(mode%2)],Pol)
                     
        elif self.EPol==True:
            print "No polarisation in Pulse_2D_kSpace"
            kCoff=numpy.ravel(numpy.column_stack((kCoff,kCoff)))
        
        kCoff=kCoff/numpy.linalg.norm(kCoff)
        return kCoff
        
        
        
    def Pulse_3D_kSpace(self,k_PulseMag,k_PulseDir,Width_para,Width_perp,r0,Pol=0): #Needs testing!!!!!!!!!!!!
        
        
        #from http://math.stackexchange.com/questions/180418/calculate-rotation-matrix-to-align-vector-a-to-vector-b-in-3d
        k_PulseDir=k_PulseDir/numpy.linalg.norm(k_PulseDir)
        v=numpy.cross(k_PulseDir,numpy.array([1.0,0.0,0.0]))
        s=numpy.linalg.norm(v)
        c=numpy.dot(k_PulseDir,numpy.array([1.0,0.0,0.0]))
        
        if c>0.9999:
            U_kvec=numpy.eye(3)
        elif c<(-1)*0.9999:
            U_kvec=-numpy.eye(3)
        else:
            vx=numpy.array([[0,-v[2],v[1]],[v[2],0,-v[0]],[-v[1],v[0],0]])
            U_kvec=numpy.eye(3)+vx+(numpy.dot(vx,vx)*((1-c)/s**2)) #rotates so ring norm in at Norm
        
        
        R0=numpy.dot(r0,U_kvec)
        
#         k_PulseVec=(k_PulseDir*k_PulseMag)/numpy.linalg.norm(k_PulseDir)
        
        
        kCoff=numpy.zeros(shape=(self.nPhot),dtype=complex)
        
        for Mode in range(0,self.nPhot,1):
            
            holder_para=numpy.exp(-((numpy.dot(self.kVec[Mode],U_kvec)[0]-k_PulseMag)**2)*(Width_para**2))*numpy.exp(-1j*R0[0]*(numpy.dot(self.kVec[Mode],U_kvec)[0]+k_PulseMag))
            holder_perp1=numpy.exp(-((numpy.dot(self.kVec[Mode],U_kvec)[1]-0)**2)*(Width_perp**2))*numpy.exp(-1j*R0[1]*(numpy.dot(self.kVec[Mode],U_kvec)[1]+0))
            holder_perp2=numpy.exp(-((numpy.dot(self.kVec[Mode],U_kvec)[2]-0)**2)*(Width_perp**2))*numpy.exp(-1j*R0[2]*(numpy.dot(self.kVec[Mode],U_kvec)[2]+0))
            
            
            kCoff[Mode]=holder_para*holder_perp1*holder_perp2
        
        
        
        if self.EPol==False:
            pass
         
        elif self.EPol==True and type(Pol)!=int:
            if numpy.dot(Pol,k_PulseDir)>0.0001:
                print "Pol and k_PulseDir not perpendicular in Pulse_3D_kSpace"
            Pol=Pol/numpy.linalg.norm(Pol)
            kCoff=numpy.ravel(numpy.column_stack((kCoff,kCoff)))
            for mode in range(0,self.nPhotE,1):
                    kCoff[mode]=kCoff[mode]*numpy.dot(self.ElecVec[int(mode/2),int(mode%2)],Pol)
                     
        elif self.EPol==True:
            print "No polarisation in Pulse_3D_kSpace"
            kCoff=numpy.ravel(numpy.column_stack((kCoff,kCoff)))
        
        kCoff=kCoff/numpy.linalg.norm(kCoff)
        return kCoff
    
    
    def ThermalState(self,Temp,Dir=0,Pol=0):
        kb=(1.3806*(10**-23))/(4.35974417*(10**-18))
        if Dir==0 or Dir in ("None", None, "0", "none"):
            if Temp>0 and Dir==0:
                kCoff=numpy.exp(-(self.Omega)/(Temp*kb))
            elif Temp==0:
                kCoff=numpy.ones(shape=(self.nPhot))
            elif Temp<0:
                if self.Dim=="1D":
                    kCoff=numpy.ones(shape=(self.nPhot))
                elif self.Dim=="2D":
                    kCoff=numpy.array([Omega**-1 for Omega in self.Omega])
                elif self.Dim=="3D":
                    kCoff=numpy.array([Omega**-2 for Omega in self.Omega])

        elif Dir in ("x","y","z","-x","-y","-z"):
            kCoff=numpy.zeros(shape=(self.nPhot))
            if Dir=="x":
                RefUniVec=numpy.array([1.0,0.0,0.0])
            elif Dir=="y":
                RefUniVec=numpy.array([0.0,1.0,0.0])
            elif Dir=="z":
                RefUniVec=numpy.array([0.0,0.0,1.0])
            elif Dir=="-x":
                RefUniVec=numpy.array([-1.0,0.0,0.0])
            elif Dir=="-y":
                RefUniVec=numpy.array([0.0,-1.0,0.0])
            elif Dir=="-z":
                RefUniVec=numpy.array([0.0,0.0,-1.0])
            
            for mode in range(0,self.nPhot,1):
                #VecMag=numpy.absolute(numpy.dot(RefUniVec,self.kVec[mode])) #Both directions
                VecMag=numpy.dot(RefUniVec,self.kVec[mode]) #One directions
                if VecMag<self.kMag[mode]*1.00001 and VecMag>self.kMag[mode]*0.99999:
                    if Temp>0:
                        kCoff[mode]=numpy.exp(-(self.Omega[mode])/(Temp*kb))
                    if Temp==0:
                        kCoff[mode]=1
        else:
            print "'Dir' not recognised in 'ThermalState' in 'JaynesCummings_Basis'"
            sys.exit()
        
            
        if self.EPol==True and numpy.linalg.norm(Pol)==0:
            kCoff=numpy.ravel(numpy.column_stack((kCoff,kCoff)))
        
        elif self.EPol==True and numpy.linalg.norm(Pol)>0:
            if len(Pol)!=3:
                print "Pol not length 3 in 'ThermalState' 'JaynesCummings_Basis'"
                sys.exit()
            if numpy.dot(Pol,numpy.array([0,1,0]))==numpy.linalg.norm(Pol) or numpy.dot(Pol,numpy.array([0,0,1]))==numpy.linalg.norm(Pol):
                Pol=Pol/numpy.linalg.norm(Pol)
                kCoff_Single=kCoff
                kCoff=numpy.zeros(shape=(self.nPhotE))
                for mode in range(0,self.nPhotE,1):
                    kCoff[mode]=kCoff_Single[int(mode/2)]*(numpy.dot(self.ElecVec[int(mode/2),mode%2],Pol)**2)
            else:
                print "PolDir in 'ThermalState' in 'JaynesCummings_Basis', must be [0,1,0] or [0,0,1], due to technical stuff"
                sys.exit()

        
        kCoff=kCoff/sum(kCoff)
        return kCoff
    
    
    def SetStochasticEnergy(self,n=10,FreqCentre=((20*(10**-15))**-1),FreqStDev=0.2, nLHII=1):
        #FreqCentre is frequency of site energy osscillations in hertz
        #StDev is as frac of mean
        
        au2s=2.418884326505e-17
        
        self.Stocastic=True
        self.E_Phase=numpy.random.rand(self.nSys,n)*2*math.pi
        self.E_Freq=2*math.pi*FreqCentre*((FreqStDev*numpy.random.randn(self.nSys,n))+1)*au2s
        self.UpdateStochasticSiteEnergy(0,nLHII)
        


        
        
    def ReturnStochasticSiteEnergy(self, t, nLHII=1):
        
        E0=numpy.array([0.05625,0.05725]) #Constant from TDDFT
        Delta_E0=numpy.array([1.0,0.9])*0.001322 #Constant from TDDFT
        nStochastic=self.E_Freq.shape[1]
        
        EList=numpy.zeros(shape=(self.nSys))
        if int(self.nSys%27)==nLHII:
            EList=numpy.zeros(shape=self.nSys)
            for n in range(0,self.LHII,1):
                EList[(n*27):(n*27)+18]=(numpy.sum(numpy.cos((self.E_Freq*t)+self.E_Phase),axis=1)*((nStochastic/2.0)**0.5)*Delta_E0[0])+E0[0]
                EList[(n*27):(n*27)+27]=(numpy.sum(numpy.cos((self.E_Freq*t)+self.E_Phase),axis=1)*((nStochastic/2.0)**0.5)*Delta_E0[1])+E0[1]
        elif len(E0)==2 and len(Delta_E0)==2:
            EList=(numpy.sum(numpy.cos((self.E_Freq*t)+self.E_Phase),axis=1)*((nStochastic/2.0)**0.5)*Delta_E0[0])+E0[0]
        else:
            EList=(numpy.sum(numpy.cos((self.E_Freq*t)+self.E_Phase),axis=1)*((nStochastic/2.0)**0.5)*Delta_E0)+E0
        
        return EList

    def UpdateStochasticSiteEnergy(self, t, nLHII=1):
        
        EList=self.ReturnStochasticSiteEnergy(t, nLHII)
        
        self.ReadInSysInfo(self.HamSys-numpy.diag(numpy.diagonal(self.HamSys)+numpy.diag(EList)),self.mu_Site,self.rSite,self.LocBasisShrink)
        self.BuildHam_JanesCummings_1p()
        