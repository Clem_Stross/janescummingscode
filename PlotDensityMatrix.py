import numpy
import matplotlib.pyplot as plt
import matplotlib.animation as animation

Save_Movie=True
au2fs=2.418884326505e-2

def PlotDensityMatrix(DM,tMax,tStep,pState=None,Index="l",OutputSuffix="",NumOfLHII=1,fs=True):
    
    
    
    fig, ax = plt.subplots()
    mat = ax.matshow(numpy.zeros(shape=DM[0].shape),cmap=plt.cm.gray_r)
    
    DMe=numpy.zeros(shape=DM.shape,dtype=complex)
    for step in range(0,len(DM),1):
        if numpy.absolute(numpy.trace(DM[step]))>0:
            DMe[step]=DM[step]/numpy.trace(DM[step])
    
    if Index=="l":
        PlotTitle="Site Density Matrix"
#         ax.title(PlotTitle)
        ax.set_xlabel(r'$| \Phi_{l} \rangle$')
        ax.set_ylabel(r'$\langle \Phi_{l} |$')
    elif Index=="k":
        for step in range(0,len(DM),1):
            DMe[step]=numpy.dot(numpy.dot(pState.U,DMe[step]),numpy.transpose(pState.U))
        PlotTitle="Eigenstate Density Matrix"
#         plt.title(PlotTitle)
        ax.set_xlabel(r'$| \Psi_{k} \rangle$')
        ax.set_ylabel(r'$\langle \Psi_{k} |$')
    else:
        print "'Index' must be 'l' or 'k' in Draw_DM_EigenAnal"
        sys.exit()
    
    
    if fs==True:
        tMax=tMax*au2fs
        tStep=tStep*au2fs
        tUnits=" fs "
    elif fs==False:
        tUnits=" a.u."
    
    

    mat.set_clim(0.0,numpy.amax(numpy.absolute(DMe))*1.25)
    time_text = ax.text(-1.5, -1.5,'')
    
    
    def animate(i):
        mat.set_data(numpy.absolute(DMe[i]))
        time_text.set_text("t="+str(float('%.3g' % (i*tStep)))+tUnits)
        
        return mat 
#         fig.colorbar(cax)
    
#         if len(DM)<=27:
#             plt.set_xticklabels(['']+range(1,len(DM)+1))
#             plt.set_yticklabels(['']+range(1,len(DM)+1))
        
    
    def init():
        mat.set_data(numpy.absolute(DMe[0]))
    
    plt.colorbar(mat)
    ani = animation.FuncAnimation(fig, animate, numpy.arange(1, int(tMax/tStep),1),init_func=init,interval=100, blit=False,repeat=True)
#     ani = animation.FuncAnimation(fig, animate, numpy.arange(1, int(tMax/tStep),1),interval=10, blit=False,repeat=True)


    
    
    if Save_Movie:
#         Set up formatting for the movie files
#         Writer = animation.writers['ffmpeg']
#         writer = Writer(fps=15, metadata=dict(artist='Me'), bitrate=1800)
        FFwriter = animation.FFMpegWriter()
        ani.save("./"+PlotTitle.replace(" ","").replace(",","_")+OutputSuffix+".mp4",writer = FFwriter, fps=2, extra_args=['-vcodec', 'h264','-pix_fmt', 'yuv420p'])
#         ani.save('./im.mp4', writer='ffmpeg')
        plt.close()
    else:
        plt.show()
        plt.close()
    