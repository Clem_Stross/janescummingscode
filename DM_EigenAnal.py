'''
Created on Dec 11, 2015

@author: cs9114
'''

import numpy

import matplotlib.pyplot as plt
from matplotlib.patches import Wedge
import matplotlib.animation as animation

from Localisation import Localisation_InvParLength
from DrawSystem import DrawSystem
from SitePopulation import RelSitePop

# Tableau 20 Colors
tableau20 = [(31, 119, 180), (174, 199, 232), (255, 127, 14), (255, 187, 120),  
             (44, 160, 44), (152, 223, 138), (214, 39, 40), (255, 152, 150),  
             (148, 103, 189), (197, 176, 213), (140, 86, 75), (196, 156, 148),  
             (227, 119, 194), (247, 182, 210), (127, 127, 127), (199, 199, 199),  
             (188, 189, 34), (219, 219, 141), (23, 190, 207), (158, 218, 229)]
             
# Tableau Color Blind 10
tableau20blind = [(0, 107, 164), (255, 128, 14), (171, 171, 171), (89, 89, 89),
             (95, 158, 209), (200, 82, 0), (137, 137, 137), (163, 200, 236),
             (255, 188, 121), (207, 207, 207)]


tableau20blind = [(0, 107, 164), (255, 128, 14), (171, 171, 171), (89, 89, 89),
             (95, 158, 209), (200, 82, 0), (137, 137, 137), (163, 200, 236),
             (255, 188, 121), (207, 207, 207)]

Save_Movie=True
au2fs=2.418884326505e-2

# Rescale to values between 0 and 1 
for i in range(len(tableau20)):  
    r, g, b = tableau20[i]  
    tableau20[i] = (r / 255., g / 255., b / 255.)
for i in range(len(tableau20blind)):  
    r, g, b = tableau20blind[i]  
    tableau20blind[i] = (r / 255., g / 255., b / 255.)

# ColorList=('b', 'g', 'r', 'c', 'm', 'y', 'k')
ColorList=tableau20blind
MarkerList=('+', 'x', 'o', 's', 'd')
LineList=([8, 8],[6, 4],[8, 4,4,4],[8, 4, 2, 4, 2, 4],[8, 4, 2, 4])


def DM_NaturalOrbitalAnal(SysDM):
    
    wfProb=numpy.zeros(shape=(SysDM.shape[0],SysDM.shape[1]))
    wf=numpy.zeros(shape=(SysDM.shape),dtype=complex)
    
    for t in range(0,len(SysDM),1):
        if numpy.trace(SysDM[t])>0:
            SysDM[t]=SysDM[t]/numpy.trace(SysDM[t])
        
        
            C_hold,wf_hold=numpy.linalg.eigh(SysDM[t])
            wf[t]=numpy.transpose(wf_hold)[::-1]
            for i in range(0,len(wf[t]),1):
                wf[t,i]=wf[t,i]/numpy.linalg.norm(wf[t,i])
            wfProb[t]=C_hold[::-1]
            
    
    return wfProb,wf


def DM_EigenbasisAnal(SysDM,U):
    wfProb=numpy.zeros(shape=(SysDM.shape[0],SysDM.shape[1]))
    
    for t in range(0,len(SysDM),1):
        if numpy.trace(SysDM[t])>0:
            SysDM[t]=SysDM[t]/numpy.trace(SysDM[t])
            
            wfProb[t]=numpy.absolute(numpy.dot(numpy.dot(U,SysDM[t]),numpy.transpose(U)).diagonal())
#             wfProb[t]=numpy.absolute(numpy.dot(numpy.dot(numpy.transpose(U),SysDM[t]),U).diagonal())

    
    return wfProb
    

def Plot_DM_NaturalOrbitalAnal(SysDM,tMax,tStep,pState=None,OutputSuffix="",Theshold=1*10**-1,fs=True):
    
    if fs==True:
        tMax=tMax*au2fs
        tStep=tStep*au2fs
    
    wfProb,wf=DM_NaturalOrbitalAnal(SysDM)
    
    fig = plt.figure()
    PlotEigen = plt.subplot(3,1,1)
    
    index=0
    for i in range(0,wfProb.shape[1],1):
        if numpy.amax(numpy.absolute(wfProb[1:,i]))>Theshold:
            index+=1
#             PlotEigen.plot(numpy.linspace(0,tMax,wfProb.shape[0])[1:],wfProb[1:,i],color=ColorList[-i%len(ColorList)], Marker=MarkerList[i%len(MarkerList)],linestyle="",label=r'$|\eta_{'+str(index)+r'} \rangle \langle \eta_{'+str(index)+r'} |$')
            PlotEigen.plot(numpy.linspace(0,tMax,wfProb.shape[0])[1:],wfProb[1:,i],color=ColorList[-i%len(ColorList)],linestyle="-",dashes=LineList[i%len(LineList)],label=r'$|\eta_{'+str(index)+r'} \rangle \langle \eta_{'+str(index)+r'} |$')
    PlotEigen.set_ylabel(r'$\rho_{sys}^{e}$ Eigenvalues')
    PlotEigen.axis([0,tMax+tStep,0,1.1])
    PlotEigen.legend(loc='upper center',bbox_to_anchor=(0.5, 1.4),ncol=5)
    
    
    PlotLoc=plt.subplot(3, 1, 2)
    PlotEigenStatDecomp=plt.subplot(3, 1, 3)
    index=0
    for i in range(0,wfProb.shape[1],1):
        if numpy.amax(numpy.absolute(wfProb[1:,i]))>Theshold:
            index+=1
#             Deloc=numpy.array([Localisation_InvParLength(wf[t,i]) for t in range(1,len(wf),1)])
            Deloc=Localisation_InvParLength(numpy.absolute(wf[:,i])**2)[1:]
#             PlotLoc.plot(numpy.linspace(0,tMax,wfProb.shape[0])[1:],Deloc,color=ColorList[-i%len(ColorList)], Marker=MarkerList[i%len(MarkerList)],linestyle="")
            PlotLoc.plot(numpy.linspace(0,tMax,wfProb.shape[0])[1:],Deloc,color=ColorList[-i%len(ColorList)], linestyle="-",dashes=LineList[i%len(LineList)])

            
            #Project into eigenstates
            for j in range(0,len(pState.U),1):
                Eigenstate=pState.U[j]
                EigenstateCoeff=numpy.dot(wf[:,i],Eigenstate)
#                 PlotEigenStatDecomp.plot(numpy.linspace(0,tMax,wfProb.shape[0])[1:],(numpy.absolute(EigenstateCoeff)**2)[1:],color=ColorList[-i%len(ColorList)],linestyle="-")
                
                if numpy.amax(EigenstateCoeff**2)>Theshold:
                    PlotEigenStatDecomp.plot(numpy.linspace(0,tMax,wfProb.shape[0])[1:],(numpy.absolute(EigenstateCoeff)**2)[1:],color=ColorList[-i%len(ColorList)],linestyle="-")
#                     PlotEigenStatDecomp.text((((i+1)%10)*tMax),EigenstateCoeff[int(((i+1)%10)*len(EigenstateCoeff)/10)],str(Localisation_InvParLength(numpy.absolute(Eigenstate)**2)))
#                     PlotEigenStatDecomp.text((j%15)*(tMax/15),numpy.absolute(EigenstateCoeff[int(((j+1)%14)*len(EigenstateCoeff)/15)])**2,'%s' % float('%.2g' % numpy.absolute(Localisation_InvParLength(numpy.absolute(Eigenstate)**2))))
            
            
#             if pState!=None:
#                 DrawSystem(pState,numpy.absolute(wf[:,i])**2,OutputSuffix+"State"+str(index))
            
            
    PlotLoc.set_ylabel(r'$P(t)^{-1}$')
    PlotEigenStatDecomp.set_ylabel(r'$\langle \Psi_{k}|\eta_{j} \rangle \langle \eta_{j} |  \Psi_{k} \rangle$')
    PlotLoc.axis([0,tMax+tStep,0,SysDM.shape[-1]+1])
    PlotEigenStatDecomp.axis([0,tMax+tStep,0,1.1])
    if fs==False:
        PlotEigenStatDecomp.set_xlabel("Time a.u.")
    elif fs==True:
        PlotEigenStatDecomp.set_xlabel("Time fs.")
        
    
    
    
    plt.close()
    return fig


def Draw_DM_OrbitalAnal(SysDM,tMax,tStep,pState=None,OutputSuffix="",Index="l",Theshold=1*10**-1,NumOfLHII=1,fs=True):
    
    if Index=="l":
        wfProb,wf=DM_NaturalOrbitalAnal(SysDM)
    elif Index=="k":
        wfProb=DM_EigenbasisAnal(SysDM,pState.U)
        wf=numpy.array([pState.U])
    else:
        print "'Index' must be 'l' or 'k' in Draw_DM_EigenAnal"
        sys.exit()
        
    SitePop=numpy.array([numpy.absolute(Slice.diagonal()) for Slice in SysDM])
    
    return Draw_OrbitalAnal(SitePop,wfProb,wf,tMax,tStep,pState,OutputSuffix,Index,Theshold,NumOfLHII,fs)


def Draw_WF_OrbitalAnal(SysWF,tMax,tStep,pState=None,OutputSuffix="",Index="k",Theshold=1*10**-1,NumOfLHII=1,fs=True):
    
    SitePop=RelSitePop(numpy.absolute(SysWF)**2)
    
    
    if Index=="k":
        
        wfProb=numpy.ones(shape=SysWF.shape)*(SysWF.shape[-1]**-1)
        for t in range(0,SysWF.shape[0],1):
            wfNorm=numpy.linalg.norm(SysWF[t])
            if wfNorm>0:
                wfProb[t]=(numpy.absolute(numpy.dot(pState.U,SysWF[t]))**2)
                wfProb[t]=wfProb[t]/numpy.sum(wfProb[t])
        wf=numpy.array([pState.U])
    else:
        print "'Index' must be 'k' in Draw_WF_EigenAnal"
        sys.exit()
    
    
    return Draw_OrbitalAnal(SitePop,wfProb,wf,tMax,tStep,pState,OutputSuffix,Index,Theshold,NumOfLHII,fs)



def Draw_OrbitalAnal(SitePop,wfProb,wf,tMax,tStep,pState=None,OutputSuffix="",Index="l",Theshold=1*10**-1,NumOfLHII=1,fs=True):
    
    if fs==True:
        tMax=tMax*au2fs
        tStep=tStep*au2fs
        tUnits=" fs "
    elif fs==False:
        tUnits=" a.u. "
    
    
    if Index=="l":
        PlotTitle="Density Matrix, Natural Orbitals"
        order=-1
    elif Index=="k":
        PlotTitle="Eigenstates"
        order=1
    else:
        print "'Index' must be 'l' or 'k' in Draw_DM_EigenAnal"
        sys.exit()
    
    
    
    fig = plt.figure()
    ActiveState=[]
    
    xMin=numpy.amin(pState.rSite[:,1]-pState.Ly/2)
    xMax=numpy.amax(pState.rSite[:,1]-pState.Ly/2)
    yMin=numpy.amin(pState.rSite[:,2]-pState.Lz/2)
    yMax=numpy.amax(pState.rSite[:,2]-pState.Lz/2)
    
    
    for State in range(0,pState.nSys,1):
        if numpy.amax(numpy.absolute(wfProb[1:,State]))>Theshold:
            ActiveState.append(State)
    
    def animate(t):
        plt.clf()
        plt.title(str(PlotTitle))
        plt.text(1.4*xMin, 1.4*yMin, 't='+str(float('%.3g' % (t*tStep)))+" "+tUnits, fontsize=12)
        plt.xlim((1.5*xMin,1.5*xMax))
        plt.ylim((1.5*yMin,1.5*yMax))
        
        mMerTDM=numpy.zeros(shape=(len(ActiveState),3),dtype=complex)
        
        for Site in range(0,pState.nSys,1):
            
            for i,aState in enumerate(ActiveState):
                
                Slice=Wedge((pState.rSite[Site,1]-pState.Ly/2,pState.rSite[Site,2]-pState.Lz/2),50*(numpy.absolute(wf[t%len(wf),aState,Site])**2)*wfProb[t,aState],i*(360/len(ActiveState)),(i+1)*(360/len(ActiveState)),fc=ColorList[(order*i)%len(ColorList)],edgecolor='k')
#                 Slice=Wedge((pState.rSite[Site,1],pState.rSite[Site,2]),50*(numpy.absolute(wf[t%len(wf),aState,Site])**2),i*(360/len(ActiveState)),(i+1)*(360/len(ActiveState)),fc=ColorList[(order*i)%len(ColorList)],edgecolor='k')
                fig.gca().add_artist(Slice)
                mMerTDM[i]+=pState.mu_Site[Site]*wf[t%len(wf),aState,Site]
            
            
            if NumOfLHII%27==pState.nSys and Site%27>=18:
                Circle=plt.Circle((pState.rSite[Site,1]-pState.Ly/2,pState.rSite[Site,2]-pState.Lz/2),5,fc='none',edgecolor='b')
            else:
                Circle=plt.Circle((pState.rSite[Site,1]-pState.Ly/2,pState.rSite[Site,2]-pState.Lz/2),5,fc='none',edgecolor='k')
            fig.gca().add_artist(Circle)
            
            CirclePop=plt.Circle((pState.rSite[Site,1]-pState.Ly/2,pState.rSite[Site,2]-pState.Lz/2),50*SitePop[t,Site],fc='none',edgecolor='r')
            fig.gca().add_artist(CirclePop)
        
        for i,aState in enumerate(ActiveState):
#           Need to think about!!!!!!
            plt.arrow(0, 0, numpy.real(mMerTDM[i,1])+numpy.imag(mMerTDM[i,1]), numpy.real(mMerTDM[i,2])+numpy.imag(mMerTDM[i,2]), head_width=1, head_length=1, fc=ColorList[-i%len(ColorList)], ec=ColorList[(order*i)%len(ColorList)])
    
    def init():
        plt.clf()
    
    ani = animation.FuncAnimation(fig, animate, numpy.arange(1, int(tMax/tStep),1),init_func=init,interval=100, blit=False,repeat=True)
    
    if Save_Movie:
#         Set up formatting for the movie files
#         Writer = animation.writers['ffmpeg']
#         writer = Writer(fps=15, metadata=dict(artist='Me'), bitrate=1800)
        FFwriter = animation.FFMpegWriter()
        ani.save("./"+PlotTitle.replace(" ","").replace(",","_")+OutputSuffix+".mp4",writer = FFwriter, fps=2, extra_args=['-vcodec', 'h264','-pix_fmt', 'yuv420p'])
#         ani.save('./im.mp4', writer='ffmpeg')
        plt.close()
    else:
        plt.show()
        plt.close()
    
    