'''
Created on Oct 23, 2015

@author: cs9114
'''

import numpy
import scipy
import math
from scipy import stats

import matplotlib.pyplot as plt
import matplotlib.animation as animation




def FitPhotonPop(PhotonPop,PhotonFreq,nBin=50):
    
    BinHeight,LowLim,BinSize,c=scipy.stats.histogram(PhotonFreq,nBin,weights=PhotonPop)
    
    def gaus(x,N,mu,sigma):
        return N*(1/(sigma*(2*math.pi)**0.5))*numpy.exp(-(x-mu)**2/(sigma**2))
    
    def cauchy(x,N,mu,gamma):
        return N*(1/(math.pi*gamma))*(gamma**2/((x-mu)**2+gamma**2))

    
    xdata=numpy.linspace(LowLim,LowLim+(BinSize*nBin),nBin)
    
    FitCauchy=True
    if FitCauchy==False:
        popt,pcov = scipy.optimize.curve_fit(gaus,xdata,BinHeight,p0=[1,LowLim+((BinSize*nBin)/2),((BinSize*nBin)/2)])
    elif FitCauchy==True:
        popt,pcov = scipy.optimize.curve_fit(cauchy,xdata,BinHeight,p0=[1,LowLim+((BinSize*nBin)/2),((BinSize*nBin)/2)])
    
    return popt,pcov



def PlotPhotonPop(PhotonPop,PhotonFreq,nBin=50,Fit=None):
    
    PhotonPopFig=plt.figure()
    PhotonPopSubFig=PhotonPopFig.add_subplot(111)
    
    PhotonPopSubFig.hist(PhotonFreq, nBin, weights=PhotonPop, normed=1, facecolor='green', alpha=0.75)
    PhotonPopSubFig.set_xlabel("$\omega$")
    PhotonPopSubFig.set_ylabel("$P(\omega)$")
    PhotonPopSubFig.set_xlim([numpy.amin(PhotonFreq),numpy.amax(PhotonFreq)])
    
    if Fit==None:
        plt.close()
        return PhotonPopFig
    
    BinHeight,LowLim,BinSize,c=scipy.stats.histogram(PhotonFreq,nBin,weights=PhotonPop)
    
    
    def gaus(x,N,mu,sigma):
        return N*(1/(sigma*(2*math.pi)**0.5))*numpy.exp(-(x-mu)**2/(sigma**2))
    
    def cauchy(x,N,mu,gamma):
        return N*(1/(math.pi*gamma))*(gamma**2/((x-mu)**2+gamma**2))

    
    xdata=numpy.linspace(LowLim,LowLim+(BinSize*nBin),nBin)
    
    if Fit in ("gaus","Gaus","gaussian","Gaussian"):
        popt,pcov = scipy.optimize.curve_fit(gaus,xdata,BinHeight,p0=[1,LowLim+((BinSize*nBin)/2),((BinSize*nBin)/2)])
        ydata=gaus(xdata,1,popt[1],popt[2])
        PhotonPopSubFig.text(numpy.amax(xdata)*0.65,numpy.amax(ydata)*0.75,"$\mu$="+str(float('%.3g' % popt[1]))+"$\pm$"+str(float('%.3g' % pcov[1,1]**0.5))+"\n $\sigma=$"+str(float('%.3g' % popt[2]))+"$\pm$"+str(float('%.3g' % pcov[2,2]**0.5)))
    elif Fit in ("cauchy","Cauchy"):
        popt,pcov = scipy.optimize.curve_fit(cauchy,xdata,BinHeight,p0=[1,LowLim+((BinSize*nBin)/2),((BinSize*nBin)/2)])
        ydata=cauchy(xdata,1,popt[1],popt[2])
        PhotonPopSubFig.text(numpy.amax(xdata)*0.65,numpy.amax(ydata)*0.75,"$\mu$="+str(float('%.3g' % popt[1]))+"$\pm$"+str(float('%.3g' % pcov[1,1]**0.5))+"\n $\gamma=$"+str(float('%.3g' % popt[2]))+"$\pm$"+str(float('%.3g' % pcov[2,2]**0.5)))
    
    PhotonPopSubFig.plot(xdata,ydata,"-r")
    
    
    
    plt.close()
    return PhotonPopFig

def PlotPhotonPop_Movie(PhotonPop,PhotonFreq,nBin=50):
    
    fig, ax = plt.subplots()
    
    
    ax.hist(PhotonFreq, nBin, weights=PhotonPop[0], normed=1, facecolor='green', alpha=0.75)
    
    def animate(i):
        ax.hist(PhotonFreq, nBin, weights=PhotonPop[i], normed=1, facecolor='green', alpha=0.75)  # update the data
        return ax,
    
    #Init only required for blitting to give a clean slate.
    def init():
        ax.hist(numpy.ma.array(x, mask=True))
        return ax,
    
    ani = animation.FuncAnimation(fig, animate, numpy.arange(0, 10),init_func=init, interval=25, blit=False)
    plt.show()
